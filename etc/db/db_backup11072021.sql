-- MySQL dump 10.19  Distrib 10.3.29-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ezplatform
-- ------------------------------------------------------
-- Server version	10.3.29-MariaDB-1:10.3.29+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ezbinaryfile`
--

DROP TABLE IF EXISTS `ezbinaryfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezbinaryfile` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `version` int(11) NOT NULL DEFAULT 0,
  `download_count` int(11) NOT NULL DEFAULT 0,
  `filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `original_filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_attribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezbinaryfile`
--

LOCK TABLES `ezbinaryfile` WRITE;
/*!40000 ALTER TABLE `ezbinaryfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezbinaryfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state`
--

DROP TABLE IF EXISTS `ezcobj_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_language_id` bigint(20) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `identifier` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  `priority` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcobj_state_identifier` (`group_id`,`identifier`),
  KEY `ezcobj_state_priority` (`priority`),
  KEY `ezcobj_state_lmask` (`language_mask`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state`
--

LOCK TABLES `ezcobj_state` WRITE;
/*!40000 ALTER TABLE `ezcobj_state` DISABLE KEYS */;
INSERT INTO `ezcobj_state` VALUES (1,2,2,'not_locked',3,0),(2,2,2,'locked',3,1);
/*!40000 ALTER TABLE `ezcobj_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_group`
--

DROP TABLE IF EXISTS `ezcobj_state_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_language_id` bigint(20) NOT NULL DEFAULT 0,
  `identifier` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcobj_state_group_identifier` (`identifier`),
  KEY `ezcobj_state_group_lmask` (`language_mask`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_group`
--

LOCK TABLES `ezcobj_state_group` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_group` DISABLE KEYS */;
INSERT INTO `ezcobj_state_group` VALUES (2,2,'ez_lock',3);
/*!40000 ALTER TABLE `ezcobj_state_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_group_language`
--

DROP TABLE IF EXISTS `ezcobj_state_group_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_group_language` (
  `contentobject_state_group_id` int(11) NOT NULL DEFAULT 0,
  `real_language_id` bigint(20) NOT NULL DEFAULT 0,
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `name` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_state_group_id`,`real_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_group_language`
--

LOCK TABLES `ezcobj_state_group_language` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_group_language` DISABLE KEYS */;
INSERT INTO `ezcobj_state_group_language` VALUES (2,2,'',3,'Lock');
/*!40000 ALTER TABLE `ezcobj_state_group_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_language`
--

DROP TABLE IF EXISTS `ezcobj_state_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_language` (
  `contentobject_state_id` int(11) NOT NULL DEFAULT 0,
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_state_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_language`
--

LOCK TABLES `ezcobj_state_language` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_language` DISABLE KEYS */;
INSERT INTO `ezcobj_state_language` VALUES (1,3,'','Not locked'),(2,3,'','Locked');
/*!40000 ALTER TABLE `ezcobj_state_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_link`
--

DROP TABLE IF EXISTS `ezcobj_state_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_link` (
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_state_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`contentobject_id`,`contentobject_state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_link`
--

LOCK TABLES `ezcobj_state_link` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_link` DISABLE KEYS */;
INSERT INTO `ezcobj_state_link` VALUES (1,1),(4,1),(10,1),(11,1),(12,1),(13,1),(14,1),(41,1),(42,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1);
/*!40000 ALTER TABLE `ezcobj_state_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontent_language`
--

DROP TABLE IF EXISTS `ezcontent_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontent_language` (
  `id` bigint(20) NOT NULL DEFAULT 0,
  `disabled` int(11) NOT NULL DEFAULT 0,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezcontent_language_name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontent_language`
--

LOCK TABLES `ezcontent_language` WRITE;
/*!40000 ALTER TABLE `ezcontent_language` DISABLE KEYS */;
INSERT INTO `ezcontent_language` VALUES (2,0,'eng-GB','English (United Kingdom)');
/*!40000 ALTER TABLE `ezcontent_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentbrowsebookmark`
--

DROP TABLE IF EXISTS `ezcontentbrowsebookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentbrowsebookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezcontentbrowsebookmark_location` (`node_id`),
  KEY `ezcontentbrowsebookmark_user` (`user_id`),
  KEY `ezcontentbrowsebookmark_user_location` (`user_id`,`node_id`),
  CONSTRAINT `ezcontentbrowsebookmark_location_fk` FOREIGN KEY (`node_id`) REFERENCES `ezcontentobject_tree` (`node_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ezcontentbrowsebookmark_user_fk` FOREIGN KEY (`user_id`) REFERENCES `ezuser` (`contentobject_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentbrowsebookmark`
--

LOCK TABLES `ezcontentbrowsebookmark` WRITE;
/*!40000 ALTER TABLE `ezcontentbrowsebookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcontentbrowsebookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass`
--

DROP TABLE IF EXISTS `ezcontentclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT 0,
  `always_available` int(11) NOT NULL DEFAULT 0,
  `contentobject_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT 0,
  `creator_id` int(11) NOT NULL DEFAULT 0,
  `identifier` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `initial_language_id` bigint(20) NOT NULL DEFAULT 0,
  `is_container` int(11) NOT NULL DEFAULT 0,
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `modifier_id` int(11) NOT NULL DEFAULT 0,
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `serialized_description_list` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `serialized_name_list` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `sort_field` int(11) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 1,
  `url_alias_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentclass_version` (`version`),
  KEY `ezcontentclass_identifier` (`identifier`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass`
--

LOCK TABLES `ezcontentclass` WRITE;
/*!40000 ALTER TABLE `ezcontentclass` DISABLE KEYS */;
INSERT INTO `ezcontentclass` VALUES (1,0,1,'<short_name|name>',1024392098,14,'folder',2,1,2,1448831672,14,'a3d405b81be900468eb153d774f4f0d2','a:0:{}','a:1:{s:6:\"eng-GB\";s:6:\"Folder\";}',1,1,NULL),(2,0,0,'<short_title|title>',1024392098,14,'article',2,1,3,1082454989,14,'c15b600eb9198b1924063b5a68758232',NULL,'a:2:{s:6:\"eng-GB\";s:7:\"Article\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL),(3,0,1,'<name>',1024392098,14,'user_group',2,1,3,1048494743,14,'25b4268cdcd01921b808a0d854b877ef',NULL,'a:2:{s:6:\"eng-GB\";s:10:\"User group\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL),(4,0,1,'<first_name> <last_name>',1024392098,14,'user',2,0,3,1082018364,14,'40faa822edc579b02c25f6bb7beec3ad',NULL,'a:2:{s:6:\"eng-GB\";s:4:\"User\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL),(5,0,1,'<name>',1031484992,14,'image',2,0,3,1048494784,14,'f6df12aa74e36230eb675f364fccd25a',NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL),(12,0,1,'<name>',1052385472,14,'file',2,0,3,1052385669,14,'637d58bfddf164627bdfd265733280a0',NULL,'a:2:{s:6:\"eng-GB\";s:4:\"File\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL),(42,0,1,'<name>',1435924826,14,'landing_page',2,1,2,1435924826,14,'60c03e9758465eb69d56b3afb6adf18e','a:1:{s:6:\"eng-GB\";s:0:\"\";}','a:1:{s:6:\"eng-GB\";s:12:\"Landing page\";}',2,0,''),(43,0,1,'<title>',1537166773,14,'form',2,0,2,1537166834,14,'6f7f21df775a33c1e4bbc76b48c38476','a:0:{}','a:1:{s:6:\"eng-GB\";s:4:\"Form\";}',2,0,''),(44,0,1,'<title>',1622835293,14,'axp_landing_page',2,1,2,1625959297,14,'ad52f63abd1490af09130e0e9f84dfe9','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:16:\"AXP Landing page\";}',2,0,''),(45,0,1,'<title>',1623990373,14,'axp_infopage',2,1,2,1625959322,14,'c201e9574d13266057f5cbd51ce1a4d8','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:12:\"AXP Infopage\";}',2,0,''),(46,0,1,'<title>',1623990705,14,'axp_hero',2,1,2,1623991352,14,'c03bb329e9843e19e9cb586cc6d48dfd','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:8:\"AXP Hero\";}',2,0,''),(47,0,1,'<title>',1625980685,14,'axp_notification',2,0,2,1625981072,14,'2e7fcc8a0775f05730f71a01d0f4932a','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:16:\"AXP Notification\";}',2,0,''),(48,0,1,'<title>',1625981097,14,'axp_header',2,0,2,1625981246,14,'cffafd8776a2d51719dff20bea109fb7','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:10:\"AXP Header\";}',2,0,''),(49,0,1,'<title>',1625981315,14,'axp_footer',2,0,2,1625981340,14,'7d4cae0221bff576d25704b71c3b5c7d','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:10:\"AXP Footer\";}',2,0,''),(50,0,1,'<title>',1625981420,14,'axp_link',2,0,2,1625981795,14,'206bdf721936b309f0a0a9be02a0253f','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:8:\"AXP Link\";}',2,0,'');
/*!40000 ALTER TABLE `ezcontentclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_attribute`
--

DROP TABLE IF EXISTS `ezcontentclass_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT 0,
  `can_translate` int(11) DEFAULT 1,
  `category` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `data_float1` double DEFAULT NULL,
  `data_float2` double DEFAULT NULL,
  `data_float3` double DEFAULT NULL,
  `data_float4` double DEFAULT NULL,
  `data_int1` int(11) DEFAULT NULL,
  `data_int2` int(11) DEFAULT NULL,
  `data_int3` int(11) DEFAULT NULL,
  `data_int4` int(11) DEFAULT NULL,
  `data_text1` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text2` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text3` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text4` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text5` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_type_string` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `identifier` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_information_collector` int(11) NOT NULL DEFAULT 0,
  `is_required` int(11) NOT NULL DEFAULT 0,
  `is_searchable` int(11) NOT NULL DEFAULT 0,
  `is_thumbnail` tinyint(1) NOT NULL DEFAULT 0,
  `placement` int(11) NOT NULL DEFAULT 0,
  `serialized_data_text` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `serialized_description_list` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `serialized_name_list` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentclass_attr_ccid` (`contentclass_id`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_attribute`
--

LOCK TABLES `ezcontentclass_attribute` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_attribute` DISABLE KEYS */;
INSERT INTO `ezcontentclass_attribute` VALUES (1,0,1,'',2,0,0,0,0,255,0,0,0,'New article','','','','','ezstring','title',0,1,1,0,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Title\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(4,0,1,'',1,NULL,NULL,NULL,NULL,255,0,NULL,NULL,'Folder',NULL,NULL,NULL,NULL,'ezstring','name',0,1,1,0,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:4:\"Name\";}'),(6,0,1,'',3,0,0,0,0,255,0,0,0,'','','','',NULL,'ezstring','name',0,1,1,0,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(7,0,1,'',3,0,0,0,0,255,0,0,0,'','','','',NULL,'ezstring','description',0,0,1,0,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(8,0,1,'',4,0,0,0,0,255,0,0,0,'','','','','','ezstring','first_name',0,1,1,0,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:10:\"First name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(9,0,1,'',4,0,0,0,0,255,0,0,0,'','','','','','ezstring','last_name',0,1,1,0,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:9:\"Last name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(12,0,0,'',4,0,0,0,0,7,10,0,0,'','^[^@]+$','','','','ezuser','user_account',0,1,0,0,3,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:12:\"User account\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(116,0,1,'',5,0,0,0,0,150,0,0,0,'','','','',NULL,'ezstring','name',0,1,1,0,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(117,0,1,'',5,0,0,0,0,10,0,0,0,'','','','',NULL,'ezrichtext','caption',0,0,1,0,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:7:\"Caption\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(118,0,1,'',5,0,0,0,0,10,0,0,0,'','','','',NULL,'ezimage','image',0,0,0,0,3,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(119,0,1,'',1,NULL,NULL,NULL,NULL,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext','short_description',0,0,1,0,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:17:\"Short description\";}'),(120,0,1,'',2,0,0,0,0,10,0,0,0,'','','','','','ezrichtext','intro',0,1,1,0,4,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Intro\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(121,0,1,'',2,0,0,0,0,20,0,0,0,'','','','','','ezrichtext','body',0,0,1,0,5,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"Body\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(123,0,0,'',2,0,0,0,0,0,0,0,0,'','','','','','ezboolean','enable_comments',0,0,0,0,6,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:15:\"Enable comments\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(146,0,1,'',12,0,0,0,0,0,0,0,0,'New file','','','',NULL,'ezstring','name',0,1,1,0,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(147,0,1,'',12,0,0,0,0,10,0,0,0,'','','','',NULL,'ezrichtext','description',0,0,1,0,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(148,0,1,'',12,0,0,0,0,0,0,0,0,'','','','',NULL,'ezbinaryfile','file',0,1,0,0,3,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"File\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(152,0,1,'',2,0,0,0,0,255,0,0,0,'','','','','','ezstring','short_title',0,0,1,0,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:11:\"Short title\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(153,0,1,'',2,0,0,0,0,1,0,0,0,'','','','','','ezauthor','author',0,0,0,0,3,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:6:\"Author\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(154,0,1,'',2,0,0,0,0,0,0,0,0,'','','','','','ezobjectrelation','image',0,0,1,0,7,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(155,0,1,'',1,NULL,NULL,NULL,NULL,100,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','short_name',0,0,1,0,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:10:\"Short name\";}'),(156,0,1,'',1,NULL,NULL,NULL,NULL,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext','description',0,0,1,0,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:11:\"Description\";}'),(179,0,1,'',4,0,0,0,0,10,0,0,0,'','','','','','eztext','signature',0,0,1,0,4,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:9:\"Signature\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(180,0,1,'',4,0,0,0,0,10,0,0,0,'','','','','','ezimage','image',0,0,0,0,5,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}'),(185,0,1,'content',42,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','name',0,1,1,0,10,'N;','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),(186,0,1,'content',42,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','description',0,1,1,0,20,'N;','a:1:{s:6:\"eng-GB\";s:24:\"Landing page description\";}','a:1:{s:6:\"eng-GB\";s:11:\"Description\";}'),(187,0,1,'content',42,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezlandingpage','page',0,0,0,0,30,'N;','a:1:{s:6:\"eng-GB\";s:12:\"Landing page\";}','a:1:{s:6:\"eng-GB\";s:12:\"Landing page\";}'),(188,0,1,'content',43,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','title',0,1,1,0,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),(189,0,1,'content',43,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezform','form',0,0,0,0,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:4:\"Form\";}'),(190,0,1,'content',44,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','title',0,1,1,0,1,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),(191,0,1,'content',44,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{\"availableBlocks\":[\"targeting\",\"form\",\"tag\",\"contentlist\",\"banner\",\"collection\",\"embed\",\"gallery\",\"video\",\"rss\",\"schedule\",\"richtext\",\"axp_promo_text\",\"axp_promo_content\",\"axp_hero\"],\"availableLayouts\":{\"AXP One Zone Layout\":\"default\",\"1\":\"flexible\"},\"editorMode\":\"page_view_mode\"}','ezlandingpage','page',0,0,0,0,2,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:12:\"Landing page\";}'),(192,0,1,'Promo',44,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','promo_title',0,0,1,0,3,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:11:\"Promo Title\";}'),(193,0,1,'Promo',44,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimageasset','promo_image',0,0,1,0,4,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:11:\"Promo Image\";}'),(194,0,1,'Promo',44,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext','promo_description',0,0,1,0,5,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:17:\"Promo Description\";}'),(195,0,1,'content',45,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','title',0,1,1,0,1,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),(196,0,1,'content',45,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{\"availableBlocks\":[\"targeting\",\"form\",\"tag\",\"contentlist\",\"banner\",\"collection\",\"embed\",\"gallery\",\"video\",\"rss\",\"schedule\",\"richtext\",\"axp_promo_text\",\"axp_promo_content\",\"axp_hero\"],\"availableLayouts\":{\"AXP One Zone Layout\":\"default\",\"1\":\"flexible\"},\"editorMode\":\"page_view_mode\"}','ezlandingpage','page',0,0,0,0,2,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:12:\"Landing page\";}'),(197,0,1,'Promo',45,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','promo_title',0,0,1,0,3,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:11:\"Promo Title\";}'),(198,0,1,'Promo',45,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimageasset','promo_image',0,0,1,0,4,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:11:\"Promo Image\";}'),(199,0,1,'Promo',45,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext','promo_description',0,0,1,0,5,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:17:\"Promo Description\";}'),(200,0,1,'content',46,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','title',0,1,1,0,1,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),(202,0,1,'content',46,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','cta_label',0,0,1,0,4,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:9:\"CTA Label\";}'),(203,0,0,'content',46,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimageasset','image',0,0,0,0,3,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:5:\"Image\";}'),(204,0,1,'content',46,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext','description',0,0,1,0,2,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:11:\"Description\";}'),(205,0,1,'content',46,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','cta_external_link',0,0,1,0,5,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:17:\"CTA External Link\";}'),(206,0,1,'content',46,NULL,NULL,NULL,NULL,0,2,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints><allowed-class contentclass-identifier=\"axp_infopage\"/><allowed-class contentclass-identifier=\"axp_landing_page\"/><allowed-class contentclass-identifier=\"article\"/><allowed-class contentclass-identifier=\"file\"/><allowed-class contentclass-identifier=\"form\"/><allowed-class contentclass-identifier=\"landing_page\"/></constraints><selection_type value=\"0\"/><contentobject-placement node-id=\"2\"/></related-objects>\n','ezobjectrelation','cta_internal_link',0,0,1,0,6,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:17:\"CTA Internal Link\";}'),(207,0,1,'content',47,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','title',0,1,1,0,1,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),(209,0,1,'content',47,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext','description',0,0,1,0,2,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:11:\"Description\";}'),(210,0,1,'content',47,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','cta_label',0,0,1,0,3,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:9:\"CTA Label\";}'),(211,0,1,'content',48,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','title',0,1,1,0,1,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),(212,0,1,'content',48,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints><allowed-class contentclass-identifier=\"axp_notification\"/></constraints><type value=\"2\"/><object_class value=\"\"/><selection_type value=\"0\"/><contentobject-placement/><selection_limit value=\"0\"/></related-objects>\n','ezobjectrelationlist','notifications',0,0,1,0,2,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:13:\"Notifications\";}'),(213,0,1,'content',49,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','title',0,1,1,0,1,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}'),(214,0,1,'content',49,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints><allowed-class contentclass-identifier=\"axp_notification\"/></constraints><type value=\"2\"/><object_class value=\"\"/><selection_type value=\"0\"/><contentobject-placement/><selection_limit value=\"0\"/></related-objects>\n','ezobjectrelationlist','notifications',0,0,1,0,2,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:13:\"Notifications\";}'),(215,0,1,'content',50,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints/><selection_type value=\"0\"/><contentobject-placement/></related-objects>\n','ezobjectrelation','internal_link',0,0,1,0,2,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:13:\"Internal Link\";}'),(216,0,1,'content',50,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','external_link',0,0,1,0,3,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:13:\"External link\";}'),(217,0,1,'content',50,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring','title',0,1,1,0,1,'N;','a:1:{s:6:\"eng-GB\";N;}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}');
/*!40000 ALTER TABLE `ezcontentclass_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_attribute_ml`
--

DROP TABLE IF EXISTS `ezcontentclass_attribute_ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_attribute_ml` (
  `contentclass_attribute_id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `language_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_text` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_json` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`contentclass_attribute_id`,`version`,`language_id`),
  KEY `ezcontentclass_attribute_ml_lang_fk` (`language_id`),
  CONSTRAINT `ezcontentclass_attribute_ml_lang_fk` FOREIGN KEY (`language_id`) REFERENCES `ezcontent_language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_attribute_ml`
--

LOCK TABLES `ezcontentclass_attribute_ml` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_attribute_ml` DISABLE KEYS */;
INSERT INTO `ezcontentclass_attribute_ml` VALUES (190,0,2,'Title',NULL,NULL,NULL),(191,0,2,'Landing page',NULL,NULL,NULL),(192,0,2,'Promo Title',NULL,NULL,NULL),(193,0,2,'Promo Image',NULL,NULL,NULL),(194,0,2,'Promo Description',NULL,NULL,NULL),(195,0,2,'Title',NULL,NULL,NULL),(196,0,2,'Landing page',NULL,NULL,NULL),(197,0,2,'Promo Title',NULL,NULL,NULL),(198,0,2,'Promo Image',NULL,NULL,NULL),(199,0,2,'Promo Description',NULL,NULL,NULL),(200,0,2,'Title',NULL,NULL,NULL),(201,0,2,'Landing page',NULL,NULL,NULL),(202,0,2,'CTA Label',NULL,NULL,NULL),(203,0,2,'Image',NULL,NULL,NULL),(204,0,2,'Description',NULL,NULL,NULL),(205,0,2,'CTA External Link',NULL,NULL,NULL),(206,0,2,'CTA Internal Link',NULL,NULL,NULL),(207,0,2,'Title',NULL,NULL,NULL),(209,0,2,'Description',NULL,NULL,NULL),(210,0,2,'CTA Label',NULL,NULL,NULL),(211,0,2,'Title',NULL,NULL,NULL),(212,0,2,'Notifications',NULL,NULL,NULL),(213,0,2,'Title',NULL,NULL,NULL),(214,0,2,'Notifications',NULL,NULL,NULL),(215,0,2,'Internal Link',NULL,NULL,NULL),(216,0,2,'External link',NULL,NULL,NULL),(217,0,2,'Title',NULL,NULL,NULL);
/*!40000 ALTER TABLE `ezcontentclass_attribute_ml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_classgroup`
--

DROP TABLE IF EXISTS `ezcontentclass_classgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_classgroup` (
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `contentclass_version` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`contentclass_id`,`contentclass_version`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_classgroup`
--

LOCK TABLES `ezcontentclass_classgroup` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_classgroup` DISABLE KEYS */;
INSERT INTO `ezcontentclass_classgroup` VALUES (1,0,1,'Content'),(2,0,1,'Content'),(3,0,2,'Users'),(4,0,2,'Users'),(5,0,3,'Media'),(12,0,3,'Media'),(42,0,1,'Content'),(43,0,1,'Content'),(44,0,4,'AXP'),(45,0,4,'AXP'),(46,0,4,'AXP'),(47,0,4,'AXP'),(48,0,4,'AXP'),(49,0,4,'AXP'),(50,0,4,'AXP');
/*!40000 ALTER TABLE `ezcontentclass_classgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_name`
--

DROP TABLE IF EXISTS `ezcontentclass_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_name` (
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `contentclass_version` int(11) NOT NULL DEFAULT 0,
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `language_locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`contentclass_id`,`contentclass_version`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_name`
--

LOCK TABLES `ezcontentclass_name` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_name` DISABLE KEYS */;
INSERT INTO `ezcontentclass_name` VALUES (1,0,2,'eng-GB','Folder'),(2,0,3,'eng-GB','Article'),(3,0,3,'eng-GB','User group'),(4,0,3,'eng-GB','User'),(5,0,3,'eng-GB','Image'),(12,0,3,'eng-GB','File'),(42,0,2,'eng-GB','Landing page'),(43,0,2,'eng-GB','Form'),(44,0,2,'eng-GB','AXP Landing page'),(45,0,2,'eng-GB','AXP Infopage'),(46,0,2,'eng-GB','AXP Hero'),(47,0,2,'eng-GB','AXP Notification'),(48,0,2,'eng-GB','AXP Header'),(49,0,2,'eng-GB','AXP Footer'),(50,0,2,'eng-GB','AXP Link');
/*!40000 ALTER TABLE `ezcontentclass_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclassgroup`
--

DROP TABLE IF EXISTS `ezcontentclassgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclassgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NOT NULL DEFAULT 0,
  `creator_id` int(11) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `modifier_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclassgroup`
--

LOCK TABLES `ezcontentclassgroup` WRITE;
/*!40000 ALTER TABLE `ezcontentclassgroup` DISABLE KEYS */;
INSERT INTO `ezcontentclassgroup` VALUES (1,1031216928,14,1033922106,14,'Content'),(2,1031216941,14,1033922113,14,'Users'),(3,1032009743,14,1033922120,14,'Media'),(4,1622835286,14,1622835286,14,'AXP');
/*!40000 ALTER TABLE `ezcontentclassgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject`
--

DROP TABLE IF EXISTS `ezcontentobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `current_version` int(11) DEFAULT NULL,
  `initial_language_id` bigint(20) NOT NULL DEFAULT 0,
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT 0,
  `published` int(11) NOT NULL DEFAULT 0,
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `section_id` int(11) NOT NULL DEFAULT 0,
  `status` int(11) DEFAULT 0,
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcontentobject_remote_id` (`remote_id`),
  KEY `ezcontentobject_classid` (`contentclass_id`),
  KEY `ezcontentobject_lmask` (`language_mask`),
  KEY `ezcontentobject_pub` (`published`),
  KEY `ezcontentobject_section` (`section_id`),
  KEY `ezcontentobject_currentversion` (`current_version`),
  KEY `ezcontentobject_owner` (`owner_id`),
  KEY `ezcontentobject_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject`
--

LOCK TABLES `ezcontentobject` WRITE;
/*!40000 ALTER TABLE `ezcontentobject` DISABLE KEYS */;
INSERT INTO `ezcontentobject` VALUES (1,1,9,2,3,1448889046,'Ibexa Platform',14,1448889046,'9459d3c29e15006e45197295722c7ade',1,2,0),(4,3,1,2,3,1033917596,'Users',14,1033917596,'f5c88a2209584891056f987fd965b0ba',2,1,0),(10,4,2,2,3,1072180405,'Anonymous User',14,1033920665,'faaeb9be3bd98ed09f606fc16d144eca',2,1,0),(11,3,1,2,3,1033920746,'Guest accounts',14,1033920746,'5f7f0bdb3381d6a461d8c29ff53d908f',2,1,0),(12,3,1,2,3,1033920775,'Administrator users',14,1033920775,'9b47a45624b023b1a76c73b74d704acf',2,1,0),(13,3,1,2,3,1033920794,'Editors',14,1033920794,'3c160cca19fb135f83bd02d911f04db2',2,1,0),(14,4,3,2,3,1301062024,'Administrator User',14,1033920830,'1bb4fe25487f05527efa8bfd394cecc7',2,1,0),(41,1,1,2,3,1060695457,'Media',14,1060695457,'a6e35cbcb7cd6ae4b691f3eee30cd262',3,1,0),(42,3,1,2,3,1072180330,'Anonymous Users',14,1072180330,'15b256dbea2ae72418ff5facc999e8f9',2,1,0),(49,1,1,2,3,1080220197,'Images',14,1080220197,'e7ff633c6b8e0fd3531e74c6e712bead',3,1,0),(50,1,1,2,3,1080220220,'Files',14,1080220220,'732a5acd01b51a6fe6eab448ad4138a9',3,1,0),(51,1,1,2,3,1080220233,'Multimedia',14,1080220233,'09082deb98662a104f325aaa8c4933d3',3,1,0),(52,42,1,2,3,1442481743,'Ibexa Digital Experience Platform',14,1442481743,'34720ff636e1d4ce512f762dc638e4ac',1,1,0),(53,1,1,2,3,1486473151,'Form Uploads',14,1486473151,'6797ab09a3e84316f09c4ccabce90e2d',3,1,0),(54,1,1,2,2,1537166893,'Forms',14,1537166893,'9e863fbb0fb835ce050032b4f00de61d',6,1,0),(55,1,1,2,3,1586855342,'Site skeletons',14,1586855342,'1ac4a4b7108e607682beaba14ba860c5',7,1,0),(57,44,18,2,3,1624249401,'AXP Home',14,1622836809,'6b146d64742629f73ed41f940512efca',1,1,0),(59,5,4,2,3,1624483433,'Sit amet Dolor',14,1622948256,'4cea0f9e3541e444b76e08d218c90712',3,1,0),(60,5,1,2,3,1623796085,'Lorem',14,1623796085,'6303f24423073e049623cabd5b591619',3,1,0),(63,45,1,2,3,1624034263,'Lorem Ipsum sit amet',14,1624034263,'38ed1f70a01a8b8f25d2b88c7505ed0c',1,1,0),(64,5,1,2,3,1624034225,'basura.png',14,1624034225,'a3a9cce0c70af92800c851b5f49eb45f',3,1,0),(65,1,1,2,3,1624244274,'Test Content',14,1624244274,'370fdcfbbe3d0b482d87bc19e764ea54',3,1,0),(66,1,1,2,3,1624244309,'Hero images',14,1624244309,'c9c428273f4efe8b770df61590f093df',3,1,0),(67,5,1,2,3,1624244475,'Test1',14,1624244475,'106823987ba961dce1aae9648fbe3d49',3,2,0),(68,46,2,2,3,1624245500,'Lorem ipsum dolor',14,1624244721,'b0291753161b4a7be61f122c75765c5d',3,2,0),(69,5,1,2,3,1624244667,'Test1',14,1624244667,'4a99d01ca9537da0031fd6fba0136cc4',3,2,0),(70,5,1,2,3,1624245926,'basura.png',14,1624245926,'be43fa17badb99dc4b78963c5b614e2d',3,1,0),(71,46,2,2,3,1624311325,'Lorem ipsum dolor',14,1624248941,'d802d93f76ed238e4bae4e22468662d2',3,1,0),(72,5,1,2,3,1624248872,'Captura de pantalla de 2021-06-18 09-59-26.png',14,1624248872,'c841138839825156961bf7bc1bd69cc3',3,1,0),(73,46,1,2,3,1624249205,'Integer facilisis semper',14,1624249205,'ac650dcb921b15723cf840e56ddddb1f',3,1,0),(74,5,1,2,3,1624249203,'basura.png',14,1624249203,'d1b564171a4d3ca4ab4cef2a169477d1',3,1,0),(75,5,1,2,3,1624311259,'Hero1',14,1624311259,'f3185d123d39409632f3f3804ffd13bd',3,1,0);
/*!40000 ALTER TABLE `ezcontentobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_attribute`
--

DROP TABLE IF EXISTS `ezcontentobject_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL DEFAULT 0,
  `attribute_original_id` int(11) DEFAULT 0,
  `contentclassattribute_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `data_float` double DEFAULT NULL,
  `data_int` int(11) DEFAULT NULL,
  `data_text` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `data_type_string` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `language_code` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `sort_key_int` int(11) NOT NULL DEFAULT 0,
  `sort_key_string` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentobject_attribute_co_id_ver_lang_code` (`contentobject_id`,`version`,`language_code`),
  KEY `ezcontentobject_classattr_id` (`contentclassattribute_id`),
  KEY `sort_key_string` (`sort_key_string`(191)),
  KEY `ezcontentobject_attribute_language_code` (`language_code`),
  KEY `sort_key_int` (`sort_key_int`)
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_attribute`
--

LOCK TABLES `ezcontentobject_attribute` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_attribute` DISABLE KEYS */;
INSERT INTO `ezcontentobject_attribute` VALUES (1,9,0,4,1,NULL,NULL,'Ibexa Platform','ezstring','eng-GB',3,0,'ibexa platform'),(2,9,0,119,1,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?><section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para><emphasis role=\"strong\">You are now ready to start your project.</emphasis></para></section>','ezrichtext','eng-GB',3,0,''),(7,1,0,7,4,NULL,NULL,'Main group','ezstring','eng-GB',3,0,''),(8,1,0,6,4,NULL,NULL,'Users','ezstring','eng-GB',3,0,''),(19,2,0,8,10,0,0,'Anonymous','ezstring','eng-GB',3,0,'anonymous'),(20,2,0,9,10,0,0,'User','ezstring','eng-GB',3,0,'user'),(21,2,0,12,10,0,0,'','ezuser','eng-GB',3,0,''),(22,1,0,6,11,0,0,'Guest accounts','ezstring','eng-GB',3,0,''),(23,1,0,7,11,0,0,'','ezstring','eng-GB',3,0,''),(24,1,0,6,12,0,0,'Administrator users','ezstring','eng-GB',3,0,''),(25,1,0,7,12,0,0,'','ezstring','eng-GB',3,0,''),(26,1,0,6,13,0,0,'Editors','ezstring','eng-GB',3,0,''),(27,1,0,7,13,0,0,'','ezstring','eng-GB',3,0,''),(28,3,0,8,14,0,0,'Administrator','ezstring','eng-GB',3,0,'administrator'),(29,3,0,9,14,0,0,'User','ezstring','eng-GB',3,0,'user'),(30,3,30,12,14,0,0,'','ezuser','eng-GB',3,0,''),(98,1,0,4,41,0,0,'Media','ezstring','eng-GB',3,0,''),(99,1,0,119,41,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(100,1,0,6,42,0,0,'Anonymous Users','ezstring','eng-GB',3,0,'anonymous users'),(101,1,0,7,42,0,0,'User group for the anonymous user','ezstring','eng-GB',3,0,'user group for the anonymous user'),(102,9,0,155,1,NULL,NULL,'Ibexa Platform','ezstring','eng-GB',3,0,'ibexa platform'),(103,1,0,155,41,0,0,'','ezstring','eng-GB',3,0,''),(104,9,0,156,1,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?><section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>This is the clean installation coming with Ibexa Platform.</para><para>It\'s a bare-bones setup of the Platform, an excellent foundation to build upon if you want to start your project from scratch.</para></section>','ezrichtext','eng-GB',3,0,''),(105,1,0,156,41,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(142,1,0,4,49,0,0,'Images','ezstring','eng-GB',3,0,'images'),(143,1,0,155,49,0,0,'','ezstring','eng-GB',3,0,''),(144,1,0,119,49,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(145,1,0,156,49,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(147,1,0,4,50,0,0,'Files','ezstring','eng-GB',3,0,'files'),(148,1,0,155,50,0,0,'','ezstring','eng-GB',3,0,''),(149,1,0,119,50,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(150,1,0,156,50,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(152,1,0,4,51,0,0,'Multimedia','ezstring','eng-GB',3,0,'multimedia'),(153,1,0,155,51,0,0,'','ezstring','eng-GB',3,0,''),(154,1,0,119,51,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(155,1,0,156,51,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(177,2,0,179,10,0,0,'','eztext','eng-GB',3,0,''),(178,3,0,179,14,0,0,'','eztext','eng-GB',3,0,''),(179,2,0,180,10,0,0,'','ezimage','eng-GB',3,0,''),(180,3,0,180,14,0,0,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1301057722\"><original attribute_id=\"180\" attribute_version=\"3\" attribute_language=\"eng-GB\"/></ezimage>\n','ezimage','eng-GB',3,0,''),(242,1,0,185,52,NULL,NULL,'Ibexa Digital Experience Platform','ezstring','eng-GB',3,0,'ibexa digital experience platform'),(243,1,0,186,52,NULL,NULL,'You are now ready to start your project.','ezstring','eng-GB',3,0,'you are now ready to start your project.'),(244,1,0,187,52,NULL,NULL,NULL,'ezlandingpage','eng-GB',3,0,'ibexa digital experience platform'),(245,1,0,4,53,NULL,NULL,'Form Uploads','ezstring','eng-GB',3,0,'form uploads'),(246,1,0,155,53,NULL,NULL,'form uploads','ezstring','eng-GB',3,0,'form uploads'),(247,1,0,119,53,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Folder for file uploads</para></section>\n','ezrichtext','eng-GB',3,0,''),(248,1,0,156,53,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(249,1,0,4,54,NULL,NULL,'Forms','ezstring','eng-GB',2,0,'forms'),(250,1,0,155,54,NULL,NULL,NULL,'ezstring','eng-GB',2,0,''),(251,1,0,119,54,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',2,0,''),(252,1,0,156,54,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',2,0,''),(253,1,0,4,55,NULL,NULL,'Site skeletons','ezstring','eng-GB',3,0,'site skeletons'),(254,1,0,155,55,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(255,1,0,119,55,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(256,1,0,156,55,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(259,13,0,190,57,NULL,NULL,'AXP Home','ezstring','eng-GB',3,0,'axp home'),(259,14,0,190,57,NULL,NULL,'AXP Home','ezstring','eng-GB',3,0,'axp home'),(259,15,0,190,57,NULL,NULL,'AXP Home','ezstring','eng-GB',3,0,'axp home'),(259,16,0,190,57,NULL,NULL,'AXP Home','ezstring','eng-GB',3,0,'axp home'),(259,17,0,190,57,NULL,NULL,'AXP Home','ezstring','eng-GB',3,0,'axp home'),(259,18,0,190,57,NULL,NULL,'AXP Home','ezstring','eng-GB',3,0,'axp home'),(260,13,0,191,57,NULL,NULL,NULL,'ezlandingpage','eng-GB',3,0,''),(260,14,0,191,57,NULL,NULL,NULL,'ezlandingpage','eng-GB',3,0,''),(260,15,0,191,57,NULL,NULL,NULL,'ezlandingpage','eng-GB',3,0,''),(260,16,0,191,57,NULL,NULL,NULL,'ezlandingpage','eng-GB',3,0,''),(260,17,0,191,57,NULL,NULL,NULL,'ezlandingpage','eng-GB',3,0,''),(260,18,0,191,57,NULL,NULL,NULL,'ezlandingpage','eng-GB',3,0,''),(264,1,0,116,59,NULL,NULL,'sfgdfg','ezstring','eng-GB',3,0,'sfgdfg'),(264,2,0,116,59,NULL,NULL,'sfgdfg','ezstring','eng-GB',3,0,'sfgdfg'),(264,3,0,116,59,NULL,NULL,'sfgdfg','ezstring','eng-GB',3,0,'sfgdfg'),(264,4,0,116,59,NULL,NULL,'Sit amet Dolor','ezstring','eng-GB',3,0,'sit amet dolor'),(265,1,0,117,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>sgsdfg</para></section>\n','ezrichtext','eng-GB',3,0,'sgsdfg'),(265,2,0,117,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>sgsdfg</para></section>\n','ezrichtext','eng-GB',3,0,'sgsdfg'),(265,3,0,117,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>sgsdfg</para></section>\n','ezrichtext','eng-GB',3,0,'sgsdfg'),(265,4,0,117,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Sit Amet dolor</para></section>\n','ezrichtext','eng-GB',3,0,'Sit Amet dolor'),(266,1,0,118,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"basura.png\"\n    suffix=\"png\" basename=\"basura\" dirpath=\"var/site/storage/images/6/6/2/0/266-1-eng-GB\" url=\"var/site/storage/images/6/6/2/0/266-1-eng-GB/basura.png\"\n    original_filename=\"basura.png\" mime_type=\"image/png\" width=\"1340\"\n    height=\"697\" alternative_text=\"dfgdg\" alias_key=\"1293033771\" timestamp=\"1622948256\">\n  <original attribute_id=\"266\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"697\" Width=\"1340\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(266,2,0,118,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"IMG_20210602_222637 (1).jpeg\"\n    suffix=\"jpeg\" basename=\"IMG_20210602_222637 (1)\" dirpath=\"var/site/storage/images/6/6/2/0/266-2-eng-GB\" url=\"var/site/storage/images/6/6/2/0/266-2-eng-GB/IMG_20210602_222637 (1).jpeg\"\n    original_filename=\"IMG_20210602_222637 (1).jpeg\" mime_type=\"image/jpeg\" width=\"4640\"\n    height=\"3472\" alternative_text=\"dfgdg\" alias_key=\"1293033771\" timestamp=\"1623686786\">\n  <original attribute_id=\"266\" attribute_version=\"2\" attribute_language=\"eng-GB\"/>\n  <information Height=\"3472\" Width=\"4640\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(266,3,0,118,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Hero2.jpeg\"\n    suffix=\"jpeg\" basename=\"Hero2\" dirpath=\"var/site/storage/images/6/6/2/0/266-3-eng-GB\" url=\"var/site/storage/images/6/6/2/0/266-3-eng-GB/Hero2.jpeg\"\n    original_filename=\"Hero2.jpeg\" mime_type=\"image/jpeg\" width=\"5433\"\n    height=\"3056\" alternative_text=\"dfgdg\" alias_key=\"1293033771\" timestamp=\"1624404987\">\n  <original attribute_id=\"266\" attribute_version=\"3\" attribute_language=\"eng-GB\"/>\n  <information Height=\"3056\" Width=\"5433\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(266,4,0,118,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Hero2.jpeg\"\n    suffix=\"jpeg\" basename=\"Hero2\" dirpath=\"var/site/storage/images/6/6/2/0/266-3-eng-GB\" url=\"var/site/storage/images/6/6/2/0/266-3-eng-GB/Hero2.jpeg\"\n    original_filename=\"Hero2.jpeg\" mime_type=\"image/jpeg\" width=\"5433\"\n    height=\"3056\" alternative_text=\"dfgdg\" alias_key=\"1293033771\" timestamp=\"1624483433\">\n  <original attribute_id=\"266\" attribute_version=\"4\" attribute_language=\"eng-GB\"/>\n  <information Height=\"3056\" Width=\"5433\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(267,1,0,116,60,NULL,NULL,'Lorem','ezstring','eng-GB',3,0,'lorem'),(268,1,0,117,60,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(269,1,0,118,60,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"IMG_20210522_181705 (1).jpeg\"\n    suffix=\"jpeg\" basename=\"IMG_20210522_181705 (1)\" dirpath=\"var/site/storage/images/9/6/2/0/269-1-eng-GB\" url=\"var/site/storage/images/9/6/2/0/269-1-eng-GB/IMG_20210522_181705 (1).jpeg\"\n    original_filename=\"IMG_20210522_181705 (1).jpeg\" mime_type=\"image/jpeg\" width=\"3472\"\n    height=\"4640\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1623796085\">\n  <original attribute_id=\"269\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"4640\" Width=\"3472\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(270,13,0,192,57,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(270,14,0,192,57,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(270,15,0,192,57,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(270,16,0,192,57,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(270,17,0,192,57,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(270,18,0,192,57,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(271,13,0,193,57,NULL,NULL,'null','ezimageasset','eng-GB',3,0,''),(271,14,0,193,57,NULL,NULL,'null','ezimageasset','eng-GB',3,0,''),(271,15,0,193,57,NULL,NULL,'{\"destinationContentId\":null,\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(271,16,0,193,57,NULL,NULL,'{\"destinationContentId\":null,\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(271,17,0,193,57,NULL,NULL,'{\"destinationContentId\":null,\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(271,18,0,193,57,NULL,NULL,'{\"destinationContentId\":null,\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(272,13,0,194,57,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(272,14,0,194,57,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(272,15,0,194,57,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(272,16,0,194,57,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(272,17,0,194,57,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(272,18,0,194,57,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(283,1,0,195,63,NULL,NULL,'Lorem Ipsum sit amet','ezstring','eng-GB',3,0,'lorem ipsum sit amet'),(284,1,0,196,63,NULL,NULL,NULL,'ezlandingpage','eng-GB',3,0,''),(285,1,0,197,63,NULL,NULL,'Promo lorem','ezstring','eng-GB',3,0,'promo lorem'),(286,1,0,198,63,NULL,NULL,'{\"destinationContentId\":\"64\",\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(287,1,0,199,63,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>svdsvdsvdsv</para></section>\n','ezrichtext','eng-GB',3,0,'svdsvdsvdsv'),(288,1,0,116,64,NULL,NULL,'basura.png','ezstring','eng-GB',3,0,'basura.png'),(289,1,0,117,64,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(290,1,0,118,64,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"basura.png\"\n    suffix=\"png\" basename=\"basura\" dirpath=\"var/site/storage/images/0/9/2/0/290-1-eng-GB\" url=\"var/site/storage/images/0/9/2/0/290-1-eng-GB/basura.png\"\n    original_filename=\"basura.png\" mime_type=\"image/png\" width=\"1340\"\n    height=\"697\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1624034225\">\n  <original attribute_id=\"290\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"697\" Width=\"1340\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(291,1,0,4,65,NULL,NULL,'Test Content','ezstring','eng-GB',3,0,'test content'),(292,1,0,155,65,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(293,1,0,119,65,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(294,1,0,156,65,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(295,1,0,4,66,NULL,NULL,'Hero images','ezstring','eng-GB',3,0,'hero images'),(296,1,0,155,66,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(297,1,0,119,66,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(298,1,0,156,66,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(299,1,0,116,67,NULL,NULL,'Test1','ezstring','eng-GB',3,0,'test1'),(300,1,0,117,67,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(301,1,0,118,67,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"test1.jpeg\"\n    suffix=\"jpeg\" basename=\"test1\" dirpath=\"var/site/storage/images/1/0/3/0/301-1-eng-GB\" url=\"var/site/storage/images/1/0/3/0/301-1-eng-GB/test1.jpeg\"\n    original_filename=\"test1.jpeg\" mime_type=\"image/jpeg\" width=\"7952\"\n    height=\"5304\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1624244475\">\n  <original attribute_id=\"301\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"5304\" Width=\"7952\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(302,1,0,200,68,NULL,NULL,'Lorem ipsum dolor','ezstring','eng-GB',3,0,'lorem ipsum dolor'),(302,2,0,200,68,NULL,NULL,'Lorem ipsum dolor','ezstring','eng-GB',3,0,'lorem ipsum dolor'),(303,1,0,204,68,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Consectetur adipiscing elit. Aliquam pulvinar faucibus sollicitudin. Nulla sed orci ac libero eleifend commodo quis vel justo.</para></section>\n','ezrichtext','eng-GB',3,0,'Consectetur adipiscing elit. Aliquam pulvinar faucibus sollicitudin. Nulla sed orci ac libero eleifend commodo quis vel justo.'),(303,2,0,204,68,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Consectetur adipiscing elit. Aliquam pulvinar faucibus sollicitudin. Nulla sed orci ac libero eleifend commodo quis vel justo.</para></section>\n','ezrichtext','eng-GB',3,0,'Consectetur adipiscing elit. Aliquam pulvinar faucibus sollicitudin. Nulla sed orci ac libero eleifend commodo quis vel justo.'),(304,1,0,203,68,NULL,NULL,'{\"destinationContentId\":null,\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(304,2,0,203,68,NULL,NULL,'{\"destinationContentId\":null,\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(305,1,0,202,68,NULL,NULL,'Lorem ipsum','ezstring','eng-GB',3,0,'lorem ipsum'),(305,2,0,202,68,NULL,NULL,'Lorem ipsum','ezstring','eng-GB',3,0,'lorem ipsum'),(306,1,0,205,68,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(306,2,0,205,68,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(307,1,0,206,68,NULL,63,NULL,'ezobjectrelation','eng-GB',3,63,''),(307,2,0,206,68,NULL,63,NULL,'ezobjectrelation','eng-GB',3,63,''),(308,1,0,116,69,NULL,NULL,'Test1','ezstring','eng-GB',3,0,'test1'),(309,1,0,117,69,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(310,1,0,118,69,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"test1.jpeg\"\n    suffix=\"jpeg\" basename=\"test1\" dirpath=\"var/site/storage/images/0/1/3/0/310-1-eng-GB\" url=\"var/site/storage/images/0/1/3/0/310-1-eng-GB/test1.jpeg\"\n    original_filename=\"test1.jpeg\" mime_type=\"image/jpeg\" width=\"7952\"\n    height=\"5304\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1624244667\">\n  <original attribute_id=\"310\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"5304\" Width=\"7952\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(311,1,0,116,70,NULL,NULL,'basura.png','ezstring','eng-GB',3,0,'basura.png'),(312,1,0,117,70,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(313,1,0,118,70,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"basura.png\"\n    suffix=\"png\" basename=\"basura\" dirpath=\"var/site/storage/images/3/1/3/0/313-1-eng-GB\" url=\"var/site/storage/images/3/1/3/0/313-1-eng-GB/basura.png\"\n    original_filename=\"basura.png\" mime_type=\"image/png\" width=\"1340\"\n    height=\"697\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1624245926\">\n  <original attribute_id=\"313\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"697\" Width=\"1340\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(314,1,0,200,71,NULL,NULL,'Lorem ipsum dolor','ezstring','eng-GB',3,0,'lorem ipsum dolor'),(314,2,0,200,71,NULL,NULL,'Lorem ipsum dolor','ezstring','eng-GB',3,0,'lorem ipsum dolor'),(315,1,0,204,71,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Consectetur adipiscing elit. Aliquam pulvinar faucibus sollicitudin. Nulla sed orci ac libero eleifend commodo quis vel justo.</para></section>\n','ezrichtext','eng-GB',3,0,'Consectetur adipiscing elit. Aliquam pulvinar faucibus sollicitudin. Nulla sed orci ac libero eleifend commodo quis vel justo.'),(315,2,0,204,71,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Consectetur adipiscing elit. Aliquam pulvinar faucibus sollicitudin. Nulla sed orci ac libero eleifend commodo quis vel justo.</para></section>\n','ezrichtext','eng-GB',3,0,'Consectetur adipiscing elit. Aliquam pulvinar faucibus sollicitudin. Nulla sed orci ac libero eleifend commodo quis vel justo.'),(316,1,0,203,71,NULL,NULL,'{\"destinationContentId\":\"72\",\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(316,2,0,203,71,NULL,NULL,'{\"destinationContentId\":\"75\",\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(317,1,0,202,71,NULL,NULL,'See more','ezstring','eng-GB',3,0,'see more'),(317,2,0,202,71,NULL,NULL,'See more','ezstring','eng-GB',3,0,'see more'),(318,1,0,205,71,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(318,2,0,205,71,NULL,NULL,NULL,'ezstring','eng-GB',3,0,''),(319,1,0,206,71,NULL,63,NULL,'ezobjectrelation','eng-GB',3,63,''),(319,2,0,206,71,NULL,63,NULL,'ezobjectrelation','eng-GB',3,63,''),(320,1,0,116,72,NULL,NULL,'Captura de pantalla de 2021-06-18 09-59-26.png','ezstring','eng-GB',3,0,'captura de pantalla de 2021-06-18 09-59-26.png'),(321,1,0,117,72,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(322,1,0,118,72,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Captura de pantalla de 2021-06-18 09-59-26.png\"\n    suffix=\"png\" basename=\"Captura de pantalla de 2021-06-18 09-59-26\" dirpath=\"var/site/storage/images/2/2/3/0/322-1-eng-GB\" url=\"var/site/storage/images/2/2/3/0/322-1-eng-GB/Captura de pantalla de 2021-06-18 09-59-26.png\"\n    original_filename=\"Captura de pantalla de 2021-06-18 09-59-26.png\" mime_type=\"image/png\" width=\"1366\"\n    height=\"768\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1624248872\">\n  <original attribute_id=\"322\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"768\" Width=\"1366\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(323,1,0,200,73,NULL,NULL,'Integer facilisis semper','ezstring','eng-GB',3,0,'integer facilisis semper'),(324,1,0,204,73,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Nullam commodo massa vitae nibh aliquam auctor. Sed a lectus vel mauris aliquam tincidunt. Nullam mattis ex felis.</para></section>\n','ezrichtext','eng-GB',3,0,'Nullam commodo massa vitae nibh aliquam auctor. Sed a lectus vel mauris aliquam tincidunt. Nullam mattis ex felis.'),(325,1,0,203,73,NULL,NULL,'{\"destinationContentId\":\"74\",\"alternativeText\":null,\"source\":null}','ezimageasset','eng-GB',3,0,''),(326,1,0,202,73,NULL,NULL,'See more','ezstring','eng-GB',3,0,'see more'),(327,1,0,205,73,NULL,NULL,'https://www.google.com','ezstring','eng-GB',3,0,'https://www.google.com'),(328,1,0,206,73,NULL,NULL,NULL,'ezobjectrelation','eng-GB',3,0,''),(329,1,0,116,74,NULL,NULL,'basura.png','ezstring','eng-GB',3,0,'basura.png'),(330,1,0,117,74,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(331,1,0,118,74,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"basura.png\"\n    suffix=\"png\" basename=\"basura\" dirpath=\"var/site/storage/images/1/3/3/0/331-1-eng-GB\" url=\"var/site/storage/images/1/3/3/0/331-1-eng-GB/basura.png\"\n    original_filename=\"basura.png\" mime_type=\"image/png\" width=\"1340\"\n    height=\"697\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1624249203\">\n  <original attribute_id=\"331\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"697\" Width=\"1340\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,''),(332,1,0,116,75,NULL,NULL,'Hero1','ezstring','eng-GB',3,0,'hero1'),(333,1,0,117,75,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext','eng-GB',3,0,''),(334,1,0,118,75,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Hero1.jpeg\"\n    suffix=\"jpeg\" basename=\"Hero1\" dirpath=\"var/site/storage/images/4/3/3/0/334-1-eng-GB\" url=\"var/site/storage/images/4/3/3/0/334-1-eng-GB/Hero1.jpeg\"\n    original_filename=\"Hero1.jpeg\" mime_type=\"image/jpeg\" width=\"2400\"\n    height=\"1350\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1624311259\">\n  <original attribute_id=\"334\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"1350\" Width=\"2400\" IsColor=\"1\"/>\n  <additional_data/>\n</ezimage>','ezimage','eng-GB',3,0,'');
/*!40000 ALTER TABLE `ezcontentobject_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_link`
--

DROP TABLE IF EXISTS `ezcontentobject_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentclassattribute_id` int(11) NOT NULL DEFAULT 0,
  `from_contentobject_id` int(11) NOT NULL DEFAULT 0,
  `from_contentobject_version` int(11) NOT NULL DEFAULT 0,
  `relation_type` int(11) NOT NULL DEFAULT 1,
  `to_contentobject_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ezco_link_to_co_id` (`to_contentobject_id`),
  KEY `ezco_link_from` (`from_contentobject_id`,`from_contentobject_version`,`contentclassattribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=503 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_link`
--

LOCK TABLES `ezcontentobject_link` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_link` DISABLE KEYS */;
INSERT INTO `ezcontentobject_link` VALUES (155,191,57,13,8,59),(156,191,57,13,8,0),(157,191,57,13,8,0),(158,191,57,13,8,0),(161,191,57,14,8,0),(162,191,57,14,8,0),(163,191,57,14,8,0),(164,191,57,14,8,0),(165,191,57,14,8,60),(166,191,57,14,8,0),(175,191,57,15,8,60),(176,191,57,15,8,0),(177,191,57,15,8,0),(178,191,57,15,8,57),(179,191,57,15,8,0),(180,191,57,15,8,0),(181,191,57,15,8,0),(182,191,57,15,8,0),(183,191,57,15,8,0),(184,191,57,15,8,0),(185,191,57,15,8,0),(186,191,57,15,8,0),(187,191,57,15,8,0),(256,198,63,1,16,64),(257,196,63,1,8,0),(258,191,57,16,8,60),(260,191,57,16,8,63),(261,191,57,16,8,0),(262,191,57,17,8,60),(263,191,57,17,8,63),(264,191,57,17,8,57),(265,191,57,17,8,0),(319,206,68,1,8,63),(321,206,68,2,8,63),(326,203,71,1,16,72),(327,206,71,1,8,63),(328,203,73,1,16,74),(329,191,57,18,8,60),(330,191,57,18,8,63),(331,191,57,18,8,57),(332,191,57,18,8,0),(333,191,57,18,8,0),(334,191,57,18,8,71),(335,191,57,18,8,73),(336,191,57,18,8,0),(343,206,71,2,8,63),(344,203,71,2,16,75);
/*!40000 ALTER TABLE `ezcontentobject_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_name`
--

DROP TABLE IF EXISTS `ezcontentobject_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_name` (
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `content_version` int(11) NOT NULL DEFAULT 0,
  `content_translation` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `language_id` bigint(20) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `real_translation` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`contentobject_id`,`content_version`,`content_translation`),
  KEY `ezcontentobject_name_lang_id` (`language_id`),
  KEY `ezcontentobject_name_cov_id` (`content_version`),
  KEY `ezcontentobject_name_name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_name`
--

LOCK TABLES `ezcontentobject_name` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_name` DISABLE KEYS */;
INSERT INTO `ezcontentobject_name` VALUES (1,9,'eng-GB',2,'Ibexa Platform','eng-GB'),(4,1,'eng-GB',3,'Users','eng-GB'),(10,2,'eng-GB',3,'Anonymous User','eng-GB'),(11,1,'eng-GB',3,'Guest accounts','eng-GB'),(12,1,'eng-GB',3,'Administrator users','eng-GB'),(13,1,'eng-GB',3,'Editors','eng-GB'),(14,3,'eng-GB',3,'Administrator User','eng-GB'),(41,1,'eng-GB',3,'Media','eng-GB'),(42,1,'eng-GB',3,'Anonymous Users','eng-GB'),(49,1,'eng-GB',3,'Images','eng-GB'),(50,1,'eng-GB',3,'Files','eng-GB'),(51,1,'eng-GB',3,'Multimedia','eng-GB'),(52,1,'eng-GB',2,'Ibexa Digital Experience Platform','eng-GB'),(53,1,'eng-GB',2,'Form Uploads','eng-GB'),(54,1,'eng-GB',3,'Forms','eng-GB'),(55,1,'eng-GB',3,'Site skeletons','eng-GB'),(57,13,'eng-GB',3,'AXP Home','eng-GB'),(57,14,'eng-GB',3,'AXP Home','eng-GB'),(57,15,'eng-GB',3,'AXP Home','eng-GB'),(57,16,'eng-GB',3,'AXP Home','eng-GB'),(57,17,'eng-GB',3,'AXP Home','eng-GB'),(57,18,'eng-GB',3,'AXP Home','eng-GB'),(59,1,'eng-GB',3,'sfgdfg','eng-GB'),(59,2,'eng-GB',3,'sfgdfg','eng-GB'),(59,3,'eng-GB',3,'sfgdfg','eng-GB'),(59,4,'eng-GB',3,'Sit amet Dolor','eng-GB'),(60,1,'eng-GB',3,'Lorem','eng-GB'),(63,1,'eng-GB',3,'Lorem Ipsum sit amet','eng-GB'),(64,1,'eng-GB',3,'basura.png','eng-GB'),(65,1,'eng-GB',3,'Test Content','eng-GB'),(66,1,'eng-GB',3,'Hero images','eng-GB'),(67,1,'eng-GB',3,'Test1','eng-GB'),(68,1,'eng-GB',3,'Lorem ipsum dolor','eng-GB'),(68,2,'eng-GB',3,'Lorem ipsum dolor','eng-GB'),(69,1,'eng-GB',3,'Test1','eng-GB'),(70,1,'eng-GB',3,'basura.png','eng-GB'),(71,1,'eng-GB',3,'Lorem ipsum dolor','eng-GB'),(71,2,'eng-GB',3,'Lorem ipsum dolor','eng-GB'),(72,1,'eng-GB',3,'Captura de pantalla de 2021-06-18 09-59-26.png','eng-GB'),(73,1,'eng-GB',3,'Integer facilisis semper','eng-GB'),(74,1,'eng-GB',3,'basura.png','eng-GB'),(75,1,'eng-GB',3,'Hero1','eng-GB');
/*!40000 ALTER TABLE `ezcontentobject_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_trash`
--

DROP TABLE IF EXISTS `ezcontentobject_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_trash` (
  `node_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT 0,
  `is_hidden` int(11) NOT NULL DEFAULT 0,
  `is_invisible` int(11) NOT NULL DEFAULT 0,
  `main_node_id` int(11) DEFAULT NULL,
  `modified_subnode` int(11) DEFAULT 0,
  `parent_node_id` int(11) NOT NULL DEFAULT 0,
  `path_identification_string` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `path_string` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT 0,
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_field` int(11) DEFAULT 1,
  `sort_order` int(11) DEFAULT 1,
  `trashed` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`node_id`),
  KEY `ezcobj_trash_depth` (`depth`),
  KEY `ezcobj_trash_p_node_id` (`parent_node_id`),
  KEY `ezcobj_trash_path_ident` (`path_identification_string`(50)),
  KEY `ezcobj_trash_co_id` (`contentobject_id`),
  KEY `ezcobj_trash_modified_subnode` (`modified_subnode`),
  KEY `ezcobj_trash_path` (`path_string`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_trash`
--

LOCK TABLES `ezcontentobject_trash` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_trash` DISABLE KEYS */;
INSERT INTO `ezcontentobject_trash` VALUES (42,1,9,2,0,0,42,1486473151,2,'node_2/ez_platform','/1/2/42/',0,'581da01017b80b1afb1e5e2a3081c724',1,1,1622917112),(64,67,1,4,0,0,64,1624244475,63,'media/test_content/hero_images/test1','/1/43/62/63/64/',0,'5335b3bcb245f4225eb0bd2a1554f9e4',1,1,1624244509),(65,69,1,3,0,0,65,1624244667,51,'media/images/test1','/1/43/51/65/',0,'a29f755dfc90ea1aa4e3f5c95e889589',1,1,1624245059),(66,68,2,4,0,0,66,1624244721,63,'media/test_content/hero_images/lorem_ipsum_dolor','/1/43/62/63/66/',0,'143bd9bade8d016ba6fb79025e11f6f4',2,0,1624248731);
/*!40000 ALTER TABLE `ezcontentobject_trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_tree`
--

DROP TABLE IF EXISTS `ezcontentobject_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_tree` (
  `node_id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_is_published` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT 0,
  `is_hidden` int(11) NOT NULL DEFAULT 0,
  `is_invisible` int(11) NOT NULL DEFAULT 0,
  `main_node_id` int(11) DEFAULT NULL,
  `modified_subnode` int(11) DEFAULT 0,
  `parent_node_id` int(11) NOT NULL DEFAULT 0,
  `path_identification_string` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `path_string` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT 0,
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_field` int(11) DEFAULT 1,
  `sort_order` int(11) DEFAULT 1,
  PRIMARY KEY (`node_id`),
  KEY `ezcontentobject_tree_p_node_id` (`parent_node_id`),
  KEY `ezcontentobject_tree_path_ident` (`path_identification_string`(50)),
  KEY `ezcontentobject_tree_contentobject_id_path_string` (`path_string`(191),`contentobject_id`),
  KEY `ezcontentobject_tree_co_id` (`contentobject_id`),
  KEY `ezcontentobject_tree_depth` (`depth`),
  KEY `ezcontentobject_tree_path` (`path_string`(191)),
  KEY `modified_subnode` (`modified_subnode`),
  KEY `ezcontentobject_tree_remote_id` (`remote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_tree`
--

LOCK TABLES `ezcontentobject_tree` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_tree` DISABLE KEYS */;
INSERT INTO `ezcontentobject_tree` VALUES (1,0,1,1,0,0,0,1,1624248731,1,'','/1/',0,'629709ba256fe317c3ddcee35453a96a',1,1),(2,52,1,1,1,0,0,2,1622917112,1,'node_2','/1/2/',0,'f3e90596361e31d496d4026eb624c983',8,1),(5,4,1,1,1,0,0,5,1301062024,1,'users','/1/5/',0,'3f6d92f8044aed134f32153517850f5a',1,1),(12,11,1,1,2,0,0,12,1081860719,5,'users/guest_accounts','/1/5/12/',0,'602dcf84765e56b7f999eaafd3821dd3',1,1),(13,12,1,1,2,0,0,13,1301062024,5,'users/administrator_users','/1/5/13/',0,'769380b7aa94541679167eab817ca893',1,1),(14,13,1,1,2,0,0,14,1081860719,5,'users/editors','/1/5/14/',0,'f7dda2854fc68f7c8455d9cb14bd04a9',1,1),(15,14,1,3,3,0,0,15,1301062024,13,'users/administrator_users/administrator_user','/1/5/13/15/',0,'e5161a99f733200b9ed4e80f9c16187b',1,1),(43,41,1,1,1,0,0,43,1624248731,1,'media','/1/43/',0,'75c715a51699d2d309a924eca6a95145',9,1),(44,42,1,1,2,0,0,44,1081860719,5,'users/anonymous_users','/1/5/44/',0,'4fdf0072da953bb276c0c7e0141c5c9b',9,1),(45,10,1,2,3,0,0,45,1081860719,44,'users/anonymous_users/anonymous_user','/1/5/44/45/',0,'2cf8343bee7b482bab82b269d8fecd76',9,1),(51,49,1,1,2,0,0,51,1624245059,43,'media/images','/1/43/51/',0,'1b26c0454b09bb49dfb1b9190ffd67cb',9,1),(52,50,1,1,2,0,0,52,1081860720,43,'media/files','/1/43/52/',0,'0b113a208f7890f9ad3c24444ff5988c',9,1),(53,51,1,1,2,0,0,53,1081860720,43,'media/multimedia','/1/43/53/',0,'4f18b82c75f10aad476cae5adf98c11f',9,1),(54,53,1,1,3,0,0,54,1486473151,52,'media/files/form_uploads','/1/43/52/54/',0,'0543630fa051a1e2be54dbd32da2420f',1,1),(55,54,1,1,1,0,0,55,1537166893,1,'forms','/1/55/',0,'1dad43be47e3a5c12cd06010aab65112',9,1),(56,55,1,1,2,0,0,56,1586855342,1,'site_skeletons','/1/56/',0,'9658f6deaeef9fc27300df5d5f566b37',9,1),(57,57,1,18,2,0,0,57,1622836809,2,'node_2/axp_home','/1/2/57/',0,'048ea4920b8b6d7f10eec53cd5d92548',2,0),(58,59,1,4,3,0,0,58,1622948256,51,'media/images/sit_amet_dolor','/1/43/51/58/',0,'101ebf34be2eea3a1db0744c6cf6aabb',1,1),(59,60,1,1,3,0,0,59,1623796085,51,'media/images/lorem','/1/43/51/59/',0,'e1a3ea42a9456cfa60c4e94c2b9fbc81',1,1),(60,64,1,1,3,0,0,60,1624034225,51,'media/images/basura_png','/1/43/51/60/',0,'15094d7619d1628def18510460b33c5a',1,1),(61,63,1,1,3,0,0,61,1624034263,57,'node_2/axp_home/lorem_ipsum_sit_amet','/1/2/57/61/',0,'5b6c1c26df1c9969e3f98afd1c503e34',2,0),(62,65,1,1,2,0,0,62,1624248731,43,'media/test_content','/1/43/62/',0,'40fbc0817534765477dc1ae89fc9f1d3',1,1),(63,66,1,1,3,0,0,63,1624248731,62,'media/test_content/hero_images','/1/43/62/63/',0,'b24a2d09cf1b495826b005c3b8f31751',1,1),(67,70,1,1,3,0,0,67,1624245926,51,'media/images/basura_png2','/1/43/51/67/',0,'b7d8a74aedda9631a2fcc902679d91d8',1,1),(68,72,1,1,3,0,0,68,1624248872,51,'media/images/captura_de_pantalla_de_2021_06_18_09_59_26_png','/1/43/51/68/',0,'68176c08ef1f6e491e65bcfc1c15dc70',1,1),(69,71,1,2,4,0,0,69,1624248941,63,'media/test_content/hero_images/lorem_ipsum_dolor','/1/43/62/63/69/',0,'31b29a56982e37f7f2b9e18113101c58',2,0),(70,74,1,1,3,0,0,70,1624249203,51,'media/images/basura_png3','/1/43/51/70/',0,'f6b5421cbb9b1c9219fc1114d9e20484',1,1),(71,73,1,1,4,0,0,71,1624249206,63,'media/test_content/hero_images/integer_facilisis_semper','/1/43/62/63/71/',0,'6b6f3985c1579e730ec3b0bc44d06072',2,0),(72,75,1,1,3,0,0,72,1624311259,51,'media/images/hero1','/1/43/51/72/',0,'5e07c0be5f00686820e07de51762d5a0',1,1);
/*!40000 ALTER TABLE `ezcontentobject_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_version`
--

DROP TABLE IF EXISTS `ezcontentobject_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT 0,
  `creator_id` int(11) NOT NULL DEFAULT 0,
  `initial_language_id` bigint(20) NOT NULL DEFAULT 0,
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `version` int(11) NOT NULL DEFAULT 0,
  `workflow_event_pos` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ezcobj_version_status` (`status`),
  KEY `idx_object_version_objver` (`contentobject_id`,`version`),
  KEY `ezcontobj_version_obj_status` (`contentobject_id`,`status`),
  KEY `ezcobj_version_creator_id` (`creator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=606 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_version`
--

LOCK TABLES `ezcontentobject_version` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_version` DISABLE KEYS */;
INSERT INTO `ezcontentobject_version` VALUES (4,4,0,14,2,3,0,1,0,1,1),(439,11,1033920737,14,2,3,1033920746,1,0,1,0),(440,12,1033920760,14,2,3,1033920775,1,0,1,0),(441,13,1033920786,14,2,3,1033920794,1,0,1,0),(472,41,1060695450,14,2,3,1060695457,1,0,1,0),(473,42,1072180278,14,2,3,1072180330,1,0,1,0),(474,10,1072180337,14,2,3,1072180405,1,0,2,0),(488,49,1080220181,14,2,3,1080220197,1,0,1,0),(489,50,1080220211,14,2,3,1080220220,1,0,1,0),(490,51,1080220225,14,2,3,1080220233,1,0,1,0),(499,14,1301061783,14,2,3,1301062024,1,0,3,0),(506,1,1448889045,14,2,3,1448889046,1,0,9,0),(512,52,1442481742,14,2,3,1442481743,1,0,1,0),(513,53,1486473143,14,2,3,1486473151,1,0,1,0),(514,54,1537166893,14,2,2,1537166893,1,0,1,0),(515,55,1586855342,14,2,3,1586855342,1,0,1,0),(553,59,1622948237,14,2,3,1623686787,3,0,1,0),(556,57,1622950544,14,2,3,1623796097,3,0,13,0),(558,59,1623686772,14,2,3,1624404987,3,0,2,0),(559,57,1623795805,14,2,3,1623990121,3,0,14,0),(560,60,1623796069,14,2,3,1623796085,1,0,1,0),(565,57,1623989528,14,2,3,1624036492,3,0,15,0),(571,63,1624034179,14,2,3,1624034263,1,0,1,0),(572,64,1624034225,14,2,3,1624034225,1,0,1,0),(573,57,1624036465,14,2,3,1624036595,3,0,16,0),(574,57,1624036556,14,2,3,1624249401,3,0,17,0),(578,65,1624244197,14,2,3,1624244274,1,0,1,0),(579,66,1624244302,14,2,3,1624244309,1,0,1,0),(580,67,1624244453,14,2,3,1624244475,1,0,1,0),(581,68,1624244517,14,2,3,1624245500,3,0,1,0),(582,69,1624244654,14,2,3,1624244667,1,0,1,0),(584,68,1624245092,14,2,3,1624245500,1,0,2,0),(586,70,1624245926,14,2,3,1624245926,1,0,1,0),(589,71,1624248786,14,2,3,1624311325,3,0,1,0),(590,72,1624248872,14,2,3,1624248872,1,0,1,0),(591,73,1624249058,14,2,3,1624249206,1,0,1,0),(592,74,1624249203,14,2,3,1624249203,1,0,1,0),(593,57,1624249312,14,2,3,1624249401,1,0,18,0),(595,71,1624289976,14,2,3,1624311325,1,0,2,0),(596,75,1624311240,14,2,3,1624311259,1,0,1,0),(599,59,1624404972,14,2,3,1624483434,3,0,3,0),(602,59,1624483420,14,2,3,1624483434,1,0,4,0);
/*!40000 ALTER TABLE `ezcontentobject_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdatebasedpublisher_scheduled_entries`
--

DROP TABLE IF EXISTS `ezdatebasedpublisher_scheduled_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdatebasedpublisher_scheduled_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `version_id` int(11) DEFAULT NULL,
  `version_number` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `action_timestamp` int(11) NOT NULL,
  `action` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `url_root` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_id_version_number_action` (`content_id`,`version_number`,`action`),
  KEY `content_id` (`content_id`),
  KEY `version_id` (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdatebasedpublisher_scheduled_entries`
--

LOCK TABLES `ezdatebasedpublisher_scheduled_entries` WRITE;
/*!40000 ALTER TABLE `ezdatebasedpublisher_scheduled_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezdatebasedpublisher_scheduled_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdfsfile`
--

DROP TABLE IF EXISTS `ezdfsfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdfsfile` (
  `name_hash` varchar(34) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name_trunk` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `datatype` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'application/octet-stream',
  `scope` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `size` bigint(20) unsigned NOT NULL DEFAULT 0,
  `mtime` int(11) NOT NULL DEFAULT 0,
  `expired` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name_hash`),
  KEY `ezdfsfile_name_trunk` (`name_trunk`(191)),
  KEY `ezdfsfile_expired_name` (`expired`,`name`(191)),
  KEY `ezdfsfile_name` (`name`(191)),
  KEY `ezdfsfile_mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdfsfile`
--

LOCK TABLES `ezdfsfile` WRITE;
/*!40000 ALTER TABLE `ezdfsfile` DISABLE KEYS */;
INSERT INTO `ezdfsfile` VALUES ('0e639c5e17cd54ad7e90d76292aa2d2f','var/site/storage/images/6/6/2/0/266-3-eng-GB/Hero2.jpeg','var/site/storage/images/6/6/2/0/266-3-eng-GB/Hero2.jpeg','image/jpeg','image',1195883,1624404987,0,0),('300356a0fa1531aa7072d888497310a8','var/site/storage/images/4/3/3/0/334-1-eng-GB/Hero1.jpeg','var/site/storage/images/4/3/3/0/334-1-eng-GB/Hero1.jpeg','image/jpeg','image',957548,1624311259,0,0),('3b33827e8b29e497faaee11ecc459fa9','var/site/storage/images/_aliases/feature_promo/6/6/2/0/266-2-eng-GB/IMG_20210602_222637 (1).jpeg','var/site/storage/images/_aliases/feature_promo/6/6/2/0/266-2-eng-GB/IMG_20210602_222637 (1).jpeg','image/jpeg','image',31943,1623686791,0,0),('492bdeec91201a50654a7ad267a1a9eb','var/site/storage/images/_aliases/feature_promo/9/6/2/0/269-1-eng-GB/IMG_20210522_181705 (1).jpeg','var/site/storage/images/_aliases/feature_promo/9/6/2/0/269-1-eng-GB/IMG_20210522_181705 (1).jpeg','image/jpeg','image',18098,1623796092,0,0),('69fcb70af4cdbd6d490715f3104e5ce2','var/site/storage/images/1/0/3/0/301-1-eng-GB/test1.jpeg','var/site/storage/images/1/0/3/0/301-1-eng-GB/test1.jpeg','image/jpeg','image',1921273,1624244475,0,0),('708f5b2932df60864c37d8359bb6b59c','var/site/storage/images/3/1/3/0/313-1-eng-GB/basura.png','var/site/storage/images/3/1/3/0/313-1-eng-GB/basura.png','image/png','image',45901,1624245926,0,0),('8ec3380862f0d6578055e012c5c05d53','var/site/storage/images/6/6/2/0/266-2-eng-GB/IMG_20210602_222637 (1).jpeg','var/site/storage/images/6/6/2/0/266-2-eng-GB/IMG_20210602_222637 (1).jpeg','image/jpeg','image',2762246,1623686786,0,0),('990f5e8ea5a492583201863972dfbd7e','var/site/storage/images/0/1/3/0/310-1-eng-GB/test1.jpeg','var/site/storage/images/0/1/3/0/310-1-eng-GB/test1.jpeg','image/jpeg','image',1921273,1624244667,0,0),('b9ac562cc3e4256d5fb3d1446f4b3b99','var/site/storage/images/_aliases/feature_promo/0/9/2/0/290-1-eng-GB/basura.png','var/site/storage/images/_aliases/feature_promo/0/9/2/0/290-1-eng-GB/basura.png','image/png','image',95471,1624222018,0,0),('c0a65a9d61b4aac6b27503477fef9a40','var/site/storage/images/0/9/2/0/290-1-eng-GB/basura.png','var/site/storage/images/0/9/2/0/290-1-eng-GB/basura.png','image/png','image',45901,1624034225,0,0),('cd8f3e5c4c27758e698a254d37a93abb','var/site/storage/images/2/2/3/0/322-1-eng-GB/Captura de pantalla de 2021-06-18 09-59-26.png','var/site/storage/images/2/2/3/0/322-1-eng-GB/Captura de pantalla de 2021-06-18 09-59-26.png','image/png','image',320359,1624248872,0,0),('e019b46d24beea05472b2b2c7067fdbe','var/site/storage/images/9/6/2/0/269-1-eng-GB/IMG_20210522_181705 (1).jpeg','var/site/storage/images/9/6/2/0/269-1-eng-GB/IMG_20210522_181705 (1).jpeg','image/jpeg','image',1448961,1623796085,0,0),('e87bdaa0b42afaf71dac6e5cc65a69f9','var/site/storage/images/1/3/3/0/331-1-eng-GB/basura.png','var/site/storage/images/1/3/3/0/331-1-eng-GB/basura.png','image/png','image',45901,1624249203,0,0);
/*!40000 ALTER TABLE `ezdfsfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezeditorialworkflow_markings`
--

DROP TABLE IF EXISTS `ezeditorialworkflow_markings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezeditorialworkflow_markings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `result` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workflow_id_markings` (`workflow_id`),
  CONSTRAINT `fk_ezeditorialworkflow_markings_workflow_id` FOREIGN KEY (`workflow_id`) REFERENCES `ezeditorialworkflow_workflows` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezeditorialworkflow_markings`
--

LOCK TABLES `ezeditorialworkflow_markings` WRITE;
/*!40000 ALTER TABLE `ezeditorialworkflow_markings` DISABLE KEYS */;
INSERT INTO `ezeditorialworkflow_markings` VALUES (2,2,'draft','',NULL,'[]'),(4,4,'draft','',NULL,'[]'),(5,5,'draft','',NULL,'[]'),(8,8,'draft','',NULL,'[]'),(9,9,'draft','',NULL,'[]'),(10,10,'draft','',NULL,'[]'),(11,11,'draft','',NULL,'[]'),(12,12,'draft','',NULL,'[]'),(13,13,'draft','',NULL,'[]'),(14,14,'draft','',NULL,'[]'),(15,15,'draft','',NULL,'[]'),(16,16,'draft','',NULL,'[]'),(17,17,'draft','',NULL,'[]'),(18,18,'draft','',NULL,'[]'),(19,19,'draft','',NULL,'[]'),(20,20,'draft','',NULL,'[]');
/*!40000 ALTER TABLE `ezeditorialworkflow_markings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezeditorialworkflow_transitions`
--

DROP TABLE IF EXISTS `ezeditorialworkflow_transitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezeditorialworkflow_transitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workflow_id_transitions` (`workflow_id`),
  CONSTRAINT `fk_ezeditorialworkflow_transitions_workflow_id` FOREIGN KEY (`workflow_id`) REFERENCES `ezeditorialworkflow_workflows` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezeditorialworkflow_transitions`
--

LOCK TABLES `ezeditorialworkflow_transitions` WRITE;
/*!40000 ALTER TABLE `ezeditorialworkflow_transitions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezeditorialworkflow_transitions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezeditorialworkflow_workflows`
--

DROP TABLE IF EXISTS `ezeditorialworkflow_workflows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezeditorialworkflow_workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `version_no` int(11) NOT NULL,
  `workflow_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `initial_owner_id` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_workflow_id` (`id`),
  KEY `idx_workflow_co_id_ver` (`content_id`,`version_no`),
  KEY `idx_workflow_name` (`workflow_name`),
  KEY `initial_owner_id` (`initial_owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezeditorialworkflow_workflows`
--

LOCK TABLES `ezeditorialworkflow_workflows` WRITE;
/*!40000 ALTER TABLE `ezeditorialworkflow_workflows` DISABLE KEYS */;
INSERT INTO `ezeditorialworkflow_workflows` VALUES (2,57,1,'quick_review',14,1622836765),(4,59,1,'quick_review',14,1622948237),(5,60,1,'quick_review',14,1623796069),(8,63,1,'quick_review',14,1624034179),(9,64,1,'quick_review',14,1624034225),(10,65,1,'quick_review',14,1624244197),(11,66,1,'quick_review',14,1624244302),(12,67,1,'quick_review',14,1624244453),(13,68,1,'quick_review',14,1624244517),(14,69,1,'quick_review',14,1624244654),(15,70,1,'quick_review',14,1624245926),(16,71,1,'quick_review',14,1624248786),(17,72,1,'quick_review',14,1624248872),(18,73,1,'quick_review',14,1624249058),(19,74,1,'quick_review',14,1624249203),(20,75,1,'quick_review',14,1624311240);
/*!40000 ALTER TABLE `ezeditorialworkflow_workflows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezform_field_attributes`
--

DROP TABLE IF EXISTS `ezform_field_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezform_field_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezform_field_attributes`
--

LOCK TABLES `ezform_field_attributes` WRITE;
/*!40000 ALTER TABLE `ezform_field_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezform_field_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezform_field_validators`
--

DROP TABLE IF EXISTS `ezform_field_validators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezform_field_validators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezform_field_validators`
--

LOCK TABLES `ezform_field_validators` WRITE;
/*!40000 ALTER TABLE `ezform_field_validators` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezform_field_validators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezform_fields`
--

DROP TABLE IF EXISTS `ezform_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezform_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezform_fields`
--

LOCK TABLES `ezform_fields` WRITE;
/*!40000 ALTER TABLE `ezform_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezform_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezform_form_submission_data`
--

DROP TABLE IF EXISTS `ezform_form_submission_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezform_form_submission_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_submission_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezform_form_submission_data`
--

LOCK TABLES `ezform_form_submission_data` WRITE;
/*!40000 ALTER TABLE `ezform_form_submission_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezform_form_submission_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezform_form_submissions`
--

DROP TABLE IF EXISTS `ezform_form_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezform_form_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `language_code` varchar(6) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezform_form_submissions`
--

LOCK TABLES `ezform_form_submissions` WRITE;
/*!40000 ALTER TABLE `ezform_form_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezform_form_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezform_forms`
--

DROP TABLE IF EXISTS `ezform_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezform_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) DEFAULT NULL,
  `version_no` int(11) DEFAULT NULL,
  `content_field_id` int(11) DEFAULT NULL,
  `language_code` varchar(16) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezform_forms`
--

LOCK TABLES `ezform_forms` WRITE;
/*!40000 ALTER TABLE `ezform_forms` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezform_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezgmaplocation`
--

DROP TABLE IF EXISTS `ezgmaplocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezgmaplocation` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_version` int(11) NOT NULL DEFAULT 0,
  `latitude` double NOT NULL DEFAULT 0,
  `longitude` double NOT NULL DEFAULT 0,
  `address` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`contentobject_attribute_id`,`contentobject_version`),
  KEY `latitude_longitude_key` (`latitude`,`longitude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezgmaplocation`
--

LOCK TABLES `ezgmaplocation` WRITE;
/*!40000 ALTER TABLE `ezgmaplocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezgmaplocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezimagefile`
--

DROP TABLE IF EXISTS `ezimagefile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezimagefile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `filepath` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ezimagefile_file` (`filepath`(191)),
  KEY `ezimagefile_coid` (`contentobject_attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezimagefile`
--

LOCK TABLES `ezimagefile` WRITE;
/*!40000 ALTER TABLE `ezimagefile` DISABLE KEYS */;
INSERT INTO `ezimagefile` VALUES (1,266,'var/site/storage/images/6/6/2/0/266-1-eng-GB/basura.png'),(2,266,'var/site/storage/images-versioned/images/6/6/2/0/266-1-eng-GB/basura.png'),(3,266,'var/site/storage/images/6/6/2/0/266-2-eng-GB/IMG_20210602_222637 (1).jpeg'),(4,269,'var/site/storage/images/9/6/2/0/269-1-eng-GB/IMG_20210522_181705 (1).jpeg'),(5,290,'var/site/storage/images/0/9/2/0/290-1-eng-GB/basura.png'),(6,301,'var/site/storage/images/1/0/3/0/301-1-eng-GB/test1.jpeg'),(7,310,'var/site/storage/images/0/1/3/0/310-1-eng-GB/test1.jpeg'),(8,313,'var/site/storage/images/3/1/3/0/313-1-eng-GB/basura.png'),(9,322,'var/site/storage/images/2/2/3/0/322-1-eng-GB/Captura de pantalla de 2021-06-18 09-59-26.png'),(10,331,'var/site/storage/images/1/3/3/0/331-1-eng-GB/basura.png'),(11,334,'var/site/storage/images/4/3/3/0/334-1-eng-GB/Hero1.jpeg'),(12,266,'var/site/storage/images/6/6/2/0/266-2-eng-GB/IMG_20210602_222637 (1).jpeg'),(13,266,'var/site/storage/images/6/6/2/0/266-3-eng-GB/Hero2.jpeg'),(14,266,'var/site/storage/images/6/6/2/0/266-3-eng-GB/Hero2.jpeg'),(15,266,'var/site/storage/images/6/6/2/0/266-3-eng-GB/Hero2.jpeg');
/*!40000 ALTER TABLE `ezimagefile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezkeyword`
--

DROP TABLE IF EXISTS `ezkeyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezkeyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL DEFAULT 0,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezkeyword_keyword` (`keyword`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezkeyword`
--

LOCK TABLES `ezkeyword` WRITE;
/*!40000 ALTER TABLE `ezkeyword` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezkeyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezkeyword_attribute_link`
--

DROP TABLE IF EXISTS `ezkeyword_attribute_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezkeyword_attribute_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL DEFAULT 0,
  `objectattribute_id` int(11) NOT NULL DEFAULT 0,
  `version` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ezkeyword_attr_link_oaid` (`objectattribute_id`),
  KEY `ezkeyword_attr_link_kid_oaid` (`keyword_id`,`objectattribute_id`),
  KEY `ezkeyword_attr_link_oaid_ver` (`objectattribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezkeyword_attribute_link`
--

LOCK TABLES `ezkeyword_attribute_link` WRITE;
/*!40000 ALTER TABLE `ezkeyword_attribute_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezkeyword_attribute_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmedia`
--

DROP TABLE IF EXISTS `ezmedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmedia` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `version` int(11) NOT NULL DEFAULT 0,
  `controls` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `has_controller` int(11) DEFAULT 0,
  `height` int(11) DEFAULT NULL,
  `is_autoplay` int(11) DEFAULT 0,
  `is_loop` int(11) DEFAULT 0,
  `mime_type` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `original_filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `pluginspage` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `quality` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  PRIMARY KEY (`contentobject_attribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmedia`
--

LOCK TABLES `ezmedia` WRITE;
/*!40000 ALTER TABLE `ezmedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznode_assignment`
--

DROP TABLE IF EXISTS `eznode_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznode_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `from_node_id` int(11) DEFAULT 0,
  `is_main` int(11) NOT NULL DEFAULT 0,
  `op_code` int(11) NOT NULL DEFAULT 0,
  `parent_node` int(11) DEFAULT NULL,
  `parent_remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `remote_id` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  `sort_field` int(11) DEFAULT 1,
  `sort_order` int(11) DEFAULT 1,
  `priority` int(11) NOT NULL DEFAULT 0,
  `is_hidden` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `eznode_assignment_is_main` (`is_main`),
  KEY `eznode_assignment_coid_cov` (`contentobject_id`,`contentobject_version`),
  KEY `eznode_assignment_parent_node` (`parent_node`),
  KEY `eznode_assignment_co_version` (`contentobject_version`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznode_assignment`
--

LOCK TABLES `eznode_assignment` WRITE;
/*!40000 ALTER TABLE `eznode_assignment` DISABLE KEYS */;
INSERT INTO `eznode_assignment` VALUES (4,8,2,0,1,2,5,'','0',1,1,0,0),(5,42,1,0,1,2,5,'','0',9,1,0,0),(6,10,2,-1,1,2,44,'','0',9,1,0,0),(7,4,1,0,1,2,1,'','0',1,1,0,0),(8,12,1,0,1,2,5,'','0',1,1,0,0),(9,13,1,0,1,2,5,'','0',1,1,0,0),(11,41,1,0,1,2,1,'','0',1,1,0,0),(12,11,1,0,1,2,5,'','0',1,1,0,0),(27,49,1,0,1,2,43,'','0',9,1,0,0),(28,50,1,0,1,2,43,'','0',9,1,0,0),(29,51,1,0,1,2,43,'','0',9,1,0,0),(38,14,3,-1,1,2,13,'','0',1,1,0,0),(40,53,1,0,1,2,52,'0543630fa051a1e2be54dbd32da2420f','0',1,1,0,0),(41,54,1,0,1,2,2,'1dad43be47e3a5c12cd06010aab65112','0',9,1,0,0),(42,55,1,0,1,2,2,'9658f6deaeef9fc27300df5d5f566b37','0',9,1,0,0),(43,56,1,0,1,3,2,'7dc29bfd0aab97149b3ffc3dbaff9aec','0',2,0,0,0),(45,58,1,0,1,3,51,'5c31a3d49b6e7cd81c4dbcbb9a78fba9','0',1,1,0,0),(46,59,1,0,1,2,51,'101ebf34be2eea3a1db0744c6cf6aabb','0',1,1,0,0),(47,60,1,0,1,2,51,'e1a3ea42a9456cfa60c4e94c2b9fbc81','0',1,1,0,0),(48,61,1,0,1,3,2,'b72c8e1f2a5b73507716dfcb84d2bfb2','0',2,0,0,0),(49,62,1,0,1,3,57,'7978735d90b4eac04a4ae8cb8291c0c1','0',2,0,0,0),(50,63,1,0,1,2,57,'5b6c1c26df1c9969e3f98afd1c503e34','0',2,0,0,0),(51,64,1,0,1,2,51,'15094d7619d1628def18510460b33c5a','0',1,1,0,0),(52,65,1,0,1,2,43,'40fbc0817534765477dc1ae89fc9f1d3','0',1,1,0,0),(53,66,1,0,1,2,62,'b24a2d09cf1b495826b005c3b8f31751','0',1,1,0,0),(54,67,1,0,1,2,63,'5335b3bcb245f4225eb0bd2a1554f9e4','0',1,1,0,0),(55,68,1,0,1,2,63,'143bd9bade8d016ba6fb79025e11f6f4','0',2,0,0,0),(56,69,1,0,1,2,51,'a29f755dfc90ea1aa4e3f5c95e889589','0',1,1,0,0),(57,70,1,0,1,2,51,'b7d8a74aedda9631a2fcc902679d91d8','0',1,1,0,0),(58,71,1,0,1,2,63,'31b29a56982e37f7f2b9e18113101c58','0',2,0,0,0),(59,72,1,0,1,2,51,'68176c08ef1f6e491e65bcfc1c15dc70','0',1,1,0,0),(60,73,1,0,1,2,63,'6b6f3985c1579e730ec3b0bc44d06072','0',2,0,0,0),(61,74,1,0,1,2,51,'f6b5421cbb9b1c9219fc1114d9e20484','0',1,1,0,0),(62,75,1,0,1,2,51,'5e07c0be5f00686820e07de51762d5a0','0',1,1,0,0);
/*!40000 ALTER TABLE `eznode_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznotification`
--

DROP TABLE IF EXISTS `eznotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL DEFAULT 0,
  `is_pending` tinyint(1) NOT NULL DEFAULT 1,
  `type` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `created` int(11) NOT NULL DEFAULT 0,
  `data` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eznotification_owner_is_pending` (`owner_id`,`is_pending`),
  KEY `eznotification_owner` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznotification`
--

LOCK TABLES `eznotification` WRITE;
/*!40000 ALTER TABLE `eznotification` DISABLE KEYS */;
/*!40000 ALTER TABLE `eznotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpackage`
--

DROP TABLE IF EXISTS `ezpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpackage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `install_date` int(11) NOT NULL DEFAULT 0,
  `name` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `version` varchar(30) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpackage`
--

LOCK TABLES `ezpackage` WRITE;
/*!40000 ALTER TABLE `ezpackage` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpackage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_attributes`
--

DROP TABLE IF EXISTS `ezpage_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10298 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_attributes`
--

LOCK TABLES `ezpage_attributes` WRITE;
/*!40000 ALTER TABLE `ezpage_attributes` DISABLE KEYS */;
INSERT INTO `ezpage_attributes` VALUES (1,'content','<p>This is the clean installation coming with Ibexa Digital Experience Platform.<br>It\'s a bare-bones setup of the Platform, an excellent foundation to build upon if you want to start your project from scratch.</p>'),(2765,'title','Sit amet'),(2766,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum sit amet <emphasis role=\"strong\">dolor</emphasis></para></section>\n'),(2767,'image','59'),(2768,'cta_label','Ver más'),(2769,'cta_link','https://www.google.com'),(2770,'color','Default'),(2771,'image_left','No'),(2772,'title','Ricardo Cermeño'),(2773,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><title ezxhtml:level=\"3\">Subtitle</title><para><literallayout class=\"normal\">Hola mundo jasf kajsfk jsdfh sdfkadsf \n </literallayout></para><para> </para><itemizedlist><listitem><para>LOrem sit </para></listitem><listitem><para>Amet </para></listitem><listitem><para>DOlor</para></listitem></itemizedlist></section>\n'),(2774,'image',NULL),(2775,'cta_label','Lorem ipsum'),(2776,'cta_link','https://www.google.com'),(2777,'color','Green'),(2778,'image_left','Si'),(2779,'title','Vestibulum non mattis nibh.'),(2780,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Sed efficitur porttitor libero vel faucibus. Proin bibendum sit amet odio in facilisis. Donec suscipit in mi in commodo. Mauris at hendrerit lectus. Donec imperdiet pellentesque odio, a ornare <emphasis role=\"strong\">mauris</emphasis> blandit non. Donec efficitur tempor vehicula. Vivamus fringilla feugiat efficitur. Integer id commodo tortor. Vivamus laoreet tortor at turpis rhoncus sollicitudin.</para></section>\n'),(2781,'image',NULL),(2782,'cta_label','Sit amet'),(2783,'cta_link','https://www.google.com'),(2784,'color','Red'),(2785,'image_left','No'),(2912,'title','Sit amet'),(2913,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum sit amet <emphasis role=\"strong\">dolor</emphasis></para></section>\n'),(2914,'image','60'),(2915,'cta_label','Ver más'),(2916,'cta_link','https://www.google.com'),(2917,'color','Default'),(2918,'image_left','No'),(2919,'title','Ricardo Cermeño'),(2920,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><title ezxhtml:level=\"3\">Subtitle</title><para><literallayout class=\"normal\">Hola mundo jasf kajsfk jsdfh sdfkadsf \n </literallayout></para><para> </para><itemizedlist><listitem><para>LOrem sit </para></listitem><listitem><para>Amet </para></listitem><listitem><para>DOlor</para></listitem></itemizedlist></section>\n'),(2921,'image',NULL),(2922,'cta_label','Lorem ipsum'),(2923,'cta_link','https://www.google.com'),(2924,'color','Green'),(2925,'image_left','Si'),(2926,'title','Vestibulum non mattis nibh.'),(2927,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Sed efficitur porttitor libero vel faucibus. Proin bibendum sit amet odio in facilisis. Donec suscipit in mi in commodo. Mauris at hendrerit lectus. Donec imperdiet pellentesque odio, a ornare <emphasis role=\"strong\">mauris</emphasis> blandit non. Donec efficitur tempor vehicula. Vivamus fringilla feugiat efficitur. Integer id commodo tortor. Vivamus laoreet tortor at turpis rhoncus sollicitudin.</para></section>\n'),(2928,'image',NULL),(2929,'cta_label','Sit amet'),(2930,'cta_link','https://www.google.com'),(2931,'color','Red'),(2932,'image_left','No'),(3390,'contents','57'),(3391,'color','Default'),(3392,'image_left','No'),(3393,'title','Sit amet'),(3394,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum sit amet <emphasis role=\"strong\">dolor</emphasis></para></section>\n'),(3395,'image','60'),(3396,'cta_label','Ver más'),(3397,'cta_link','https://www.google.com'),(3398,'color','Default'),(3399,'image_left','No'),(3400,'title','Ricardo Cermeño'),(3401,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><title ezxhtml:level=\"3\">Subtitle</title><para><literallayout class=\"normal\">Hola mundo jasf kajsfk jsdfh sdfkadsf \n </literallayout></para><para> </para><itemizedlist><listitem><para>LOrem sit </para></listitem><listitem><para>Amet </para></listitem><listitem><para>DOlor</para></listitem></itemizedlist></section>\n'),(3402,'image',NULL),(3403,'cta_label','Lorem ipsum'),(3404,'cta_link','https://www.google.com'),(3405,'color','Green'),(3406,'image_left','Si'),(3407,'title','Vestibulum non mattis nibh.'),(3408,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Sed efficitur porttitor libero vel faucibus. Proin bibendum sit amet odio in facilisis. Donec suscipit in mi in commodo. Mauris at hendrerit lectus. Donec imperdiet pellentesque odio, a ornare <emphasis role=\"strong\">mauris</emphasis> blandit non. Donec efficitur tempor vehicula. Vivamus fringilla feugiat efficitur. Integer id commodo tortor. Vivamus laoreet tortor at turpis rhoncus sollicitudin.</para></section>\n'),(3409,'image',NULL),(3410,'cta_label','Sit amet'),(3411,'cta_link','https://www.google.com'),(3412,'color','Red'),(3413,'image_left','No'),(4977,'title','svs'),(4978,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>sdvsv</para></section>\n'),(4979,'image',NULL),(4980,'cta_label',NULL),(4981,'cta_link',NULL),(4982,'color','Default'),(4983,'image_left','No'),(5008,'contents','61'),(5009,'color','Default'),(5010,'image_left','No'),(5011,'title','Sit amet'),(5012,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum sit amet <emphasis role=\"strong\">dolor</emphasis></para></section>\n'),(5013,'image','60'),(5014,'cta_label','Ver más'),(5015,'cta_link','https://www.google.com'),(5016,'color','Default'),(5017,'image_left','No'),(5018,'title','Ricardo Cermeño'),(5019,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><title ezxhtml:level=\"3\">Subtitle</title><para><literallayout class=\"normal\">Hola mundo jasf kajsfk jsdfh sdfkadsf \n </literallayout></para><para> </para><itemizedlist><listitem><para>LOrem sit </para></listitem><listitem><para>Amet </para></listitem><listitem><para>DOlor</para></listitem></itemizedlist></section>\n'),(5020,'image',NULL),(5021,'cta_label','Lorem ipsum'),(5022,'cta_link','https://www.google.com'),(5023,'color','Green'),(5024,'image_left','Si'),(5025,'title','Vestibulum non mattis nibh.'),(5026,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Sed efficitur porttitor libero vel faucibus. Proin bibendum sit amet odio in facilisis. Donec suscipit in mi in commodo. Mauris at hendrerit lectus. Donec imperdiet pellentesque odio, a ornare <emphasis role=\"strong\">mauris</emphasis> blandit non. Donec efficitur tempor vehicula. Vivamus fringilla feugiat efficitur. Integer id commodo tortor. Vivamus laoreet tortor at turpis rhoncus sollicitudin.</para></section>\n'),(5027,'image',NULL),(5028,'cta_label','Sit amet'),(5029,'cta_link','https://www.google.com'),(5030,'color','Red'),(5031,'image_left','No'),(5056,'contents','61,57'),(5057,'color','Default'),(5058,'image_left','No'),(5059,'title','Sit amet'),(5060,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum sit amet <emphasis role=\"strong\">dolor</emphasis></para></section>\n'),(5061,'image','60'),(5062,'cta_label','Ver más'),(5063,'cta_link','https://www.google.com'),(5064,'color','Default'),(5065,'image_left','No'),(5066,'title','Ricardo Cermeño'),(5067,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><title ezxhtml:level=\"3\">Subtitle</title><para><literallayout class=\"normal\">Hola mundo jasf kajsfk jsdfh sdfkadsf \n </literallayout></para><para> </para><itemizedlist><listitem><para>LOrem sit </para></listitem><listitem><para>Amet </para></listitem><listitem><para>DOlor</para></listitem></itemizedlist></section>\n'),(5068,'image',NULL),(5069,'cta_label','Lorem ipsum'),(5070,'cta_link','https://www.google.com'),(5071,'color','Green'),(5072,'image_left','Si'),(5073,'title','Vestibulum non mattis nibh.'),(5074,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Sed efficitur porttitor libero vel faucibus. Proin bibendum sit amet odio in facilisis. Donec suscipit in mi in commodo. Mauris at hendrerit lectus. Donec imperdiet pellentesque odio, a ornare <emphasis role=\"strong\">mauris</emphasis> blandit non. Donec efficitur tempor vehicula. Vivamus fringilla feugiat efficitur. Integer id commodo tortor. Vivamus laoreet tortor at turpis rhoncus sollicitudin.</para></section>\n'),(5075,'image',NULL),(5076,'cta_label','Sit amet'),(5077,'cta_link','https://www.google.com'),(5078,'color','Red'),(5079,'image_left','No'),(6315,'contents','69,71'),(6316,'contents','61,57'),(6317,'color','Default'),(6318,'image_left','No'),(6319,'title','Sit amet'),(6320,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum sit amet <emphasis role=\"strong\">dolor</emphasis></para></section>\n'),(6321,'image','60'),(6322,'cta_label','Ver más'),(6323,'cta_link','https://www.google.com'),(6324,'color','Default'),(6325,'image_left','No'),(6326,'title','Ricardo Cermeño'),(6327,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><title ezxhtml:level=\"3\">Subtitle</title><para><literallayout class=\"normal\">Hola mundo jasf kajsfk jsdfh sdfkadsf \n </literallayout></para><para> </para><itemizedlist><listitem><para>LOrem sit </para></listitem><listitem><para>Amet </para></listitem><listitem><para>DOlor</para></listitem></itemizedlist></section>\n'),(6328,'image',NULL),(6329,'cta_label','Lorem ipsum'),(6330,'cta_link','https://www.google.com'),(6331,'color','Green'),(6332,'image_left','Si'),(6333,'title','Vestibulum non mattis nibh.'),(6334,'description','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Sed efficitur porttitor libero vel faucibus. Proin bibendum sit amet odio in facilisis. Donec suscipit in mi in commodo. Mauris at hendrerit lectus. Donec imperdiet pellentesque odio, a ornare <emphasis role=\"strong\">mauris</emphasis> blandit non. Donec efficitur tempor vehicula. Vivamus fringilla feugiat efficitur. Integer id commodo tortor. Vivamus laoreet tortor at turpis rhoncus sollicitudin.</para></section>\n'),(6335,'image',NULL),(6336,'cta_label','Sit amet'),(6337,'cta_link','https://www.google.com'),(6338,'color','Red'),(6339,'image_left','No');
/*!40000 ALTER TABLE `ezpage_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_blocks`
--

DROP TABLE IF EXISTS `ezpage_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `view` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2017 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_blocks`
--

LOCK TABLES `ezpage_blocks` WRITE;
/*!40000 ALTER TABLE `ezpage_blocks` DISABLE KEYS */;
INSERT INTO `ezpage_blocks` VALUES (1,'tag','default','Tag'),(706,'axp_promo_text','axp_promo_text','AXP Promo Text'),(707,'axp_promo_text','axp_promo_text','AXP Promo Text'),(708,'axp_promo_text','axp_promo_text','AXP Promo Text'),(727,'axp_promo_text','axp_promo_text','AXP Promo Text'),(728,'axp_promo_text','axp_promo_text','AXP Promo Text'),(729,'axp_promo_text','axp_promo_text','AXP Promo Text'),(807,'axp_promo_content','axp_promo_content','AXP Promo Content'),(808,'axp_promo_text','axp_promo_text','AXP Promo Text'),(809,'axp_promo_text','axp_promo_text','AXP Promo Text'),(810,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1072,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1077,'axp_promo_content','axp_promo_content','AXP Promo Content'),(1078,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1079,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1080,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1085,'axp_promo_content','axp_promo_content','AXP Promo Content'),(1086,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1087,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1088,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1298,'axp_hero','axp_hero','AXP Hero'),(1299,'axp_promo_content','axp_promo_content','AXP Promo Content'),(1300,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1301,'axp_promo_text','axp_promo_text','AXP Promo Text'),(1302,'axp_promo_text','axp_promo_text','AXP Promo Text');
/*!40000 ALTER TABLE `ezpage_blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_blocks_design`
--

DROP TABLE IF EXISTS `ezpage_blocks_design`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_blocks_design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) NOT NULL,
  `style` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `compiled` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpage_blocks_design_block_id` (`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2016 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_blocks_design`
--

LOCK TABLES `ezpage_blocks_design` WRITE;
/*!40000 ALTER TABLE `ezpage_blocks_design` DISABLE KEYS */;
INSERT INTO `ezpage_blocks_design` VALUES (705,706,NULL,'',NULL),(706,707,NULL,'',NULL),(707,708,NULL,'',NULL),(726,727,NULL,'',NULL),(727,728,NULL,'',NULL),(728,729,NULL,'',NULL),(806,807,NULL,'',NULL),(807,808,NULL,'',NULL),(808,809,NULL,'',NULL),(809,810,NULL,'',NULL),(1071,1072,NULL,'',NULL),(1076,1077,NULL,'',NULL),(1077,1078,NULL,'',NULL),(1078,1079,NULL,'',NULL),(1079,1080,NULL,'',NULL),(1084,1085,NULL,'',NULL),(1085,1086,NULL,'',NULL),(1086,1087,NULL,'',NULL),(1087,1088,NULL,'',NULL),(1297,1298,NULL,'',NULL),(1298,1299,NULL,'',NULL),(1299,1300,NULL,'',NULL),(1300,1301,NULL,'',NULL),(1301,1302,NULL,'',NULL);
/*!40000 ALTER TABLE `ezpage_blocks_design` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_blocks_visibility`
--

DROP TABLE IF EXISTS `ezpage_blocks_visibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_blocks_visibility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) NOT NULL,
  `since` int(11) DEFAULT NULL,
  `till` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpage_blocks_visibility_block_id` (`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2016 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_blocks_visibility`
--

LOCK TABLES `ezpage_blocks_visibility` WRITE;
/*!40000 ALTER TABLE `ezpage_blocks_visibility` DISABLE KEYS */;
INSERT INTO `ezpage_blocks_visibility` VALUES (705,706,NULL,NULL),(706,707,NULL,NULL),(707,708,NULL,NULL),(726,727,NULL,NULL),(727,728,NULL,NULL),(728,729,NULL,NULL),(806,807,NULL,NULL),(807,808,NULL,NULL),(808,809,NULL,NULL),(809,810,NULL,NULL),(1071,1072,NULL,NULL),(1076,1077,NULL,NULL),(1077,1078,NULL,NULL),(1078,1079,NULL,NULL),(1079,1080,NULL,NULL),(1084,1085,NULL,NULL),(1085,1086,NULL,NULL),(1086,1087,NULL,NULL),(1087,1088,NULL,NULL),(1297,1298,NULL,NULL),(1298,1299,NULL,NULL),(1299,1300,NULL,NULL),(1300,1301,NULL,NULL),(1301,1302,NULL,NULL);
/*!40000 ALTER TABLE `ezpage_blocks_visibility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_map_attributes_blocks`
--

DROP TABLE IF EXISTS `ezpage_map_attributes_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_map_attributes_blocks` (
  `attribute_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  PRIMARY KEY (`attribute_id`,`block_id`),
  KEY `ezpage_map_attributes_blocks_attribute_id` (`attribute_id`),
  KEY `ezpage_map_attributes_blocks_block_id` (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_map_attributes_blocks`
--

LOCK TABLES `ezpage_map_attributes_blocks` WRITE;
/*!40000 ALTER TABLE `ezpage_map_attributes_blocks` DISABLE KEYS */;
INSERT INTO `ezpage_map_attributes_blocks` VALUES (1,1),(2765,706),(2766,706),(2767,706),(2768,706),(2769,706),(2770,706),(2771,706),(2772,707),(2773,707),(2774,707),(2775,707),(2776,707),(2777,707),(2778,707),(2779,708),(2780,708),(2781,708),(2782,708),(2783,708),(2784,708),(2785,708),(2912,727),(2913,727),(2914,727),(2915,727),(2916,727),(2917,727),(2918,727),(2919,728),(2920,728),(2921,728),(2922,728),(2923,728),(2924,728),(2925,728),(2926,729),(2927,729),(2928,729),(2929,729),(2930,729),(2931,729),(2932,729),(3390,807),(3391,807),(3392,807),(3393,808),(3394,808),(3395,808),(3396,808),(3397,808),(3398,808),(3399,808),(3400,809),(3401,809),(3402,809),(3403,809),(3404,809),(3405,809),(3406,809),(3407,810),(3408,810),(3409,810),(3410,810),(3411,810),(3412,810),(3413,810),(4977,1072),(4978,1072),(4979,1072),(4980,1072),(4981,1072),(4982,1072),(4983,1072),(5008,1077),(5009,1077),(5010,1077),(5011,1078),(5012,1078),(5013,1078),(5014,1078),(5015,1078),(5016,1078),(5017,1078),(5018,1079),(5019,1079),(5020,1079),(5021,1079),(5022,1079),(5023,1079),(5024,1079),(5025,1080),(5026,1080),(5027,1080),(5028,1080),(5029,1080),(5030,1080),(5031,1080),(5056,1085),(5057,1085),(5058,1085),(5059,1086),(5060,1086),(5061,1086),(5062,1086),(5063,1086),(5064,1086),(5065,1086),(5066,1087),(5067,1087),(5068,1087),(5069,1087),(5070,1087),(5071,1087),(5072,1087),(5073,1088),(5074,1088),(5075,1088),(5076,1088),(5077,1088),(5078,1088),(5079,1088),(6315,1298),(6316,1299),(6317,1299),(6318,1299),(6319,1300),(6320,1300),(6321,1300),(6322,1300),(6323,1300),(6324,1300),(6325,1300),(6326,1301),(6327,1301),(6328,1301),(6329,1301),(6330,1301),(6331,1301),(6332,1301),(6333,1302),(6334,1302),(6335,1302),(6336,1302),(6337,1302),(6338,1302),(6339,1302);
/*!40000 ALTER TABLE `ezpage_map_attributes_blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_map_blocks_zones`
--

DROP TABLE IF EXISTS `ezpage_map_blocks_zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_map_blocks_zones` (
  `block_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  PRIMARY KEY (`block_id`,`zone_id`),
  KEY `ezpage_map_blocks_zones_block_id` (`block_id`),
  KEY `ezpage_map_blocks_zones_zone_id` (`zone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_map_blocks_zones`
--

LOCK TABLES `ezpage_map_blocks_zones` WRITE;
/*!40000 ALTER TABLE `ezpage_map_blocks_zones` DISABLE KEYS */;
INSERT INTO `ezpage_map_blocks_zones` VALUES (1,1),(706,398),(707,398),(708,398),(727,405),(728,405),(729,405),(807,426),(808,426),(809,426),(810,426),(1072,496),(1077,498),(1078,498),(1079,498),(1080,498),(1085,500),(1086,500),(1087,500),(1088,500),(1298,553),(1299,553),(1300,553),(1301,553),(1302,553);
/*!40000 ALTER TABLE `ezpage_map_blocks_zones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_map_zones_pages`
--

DROP TABLE IF EXISTS `ezpage_map_zones_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_map_zones_pages` (
  `zone_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`zone_id`,`page_id`),
  KEY `ezpage_map_zones_pages_zone_id` (`zone_id`),
  KEY `ezpage_map_zones_pages_page_id` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_map_zones_pages`
--

LOCK TABLES `ezpage_map_zones_pages` WRITE;
/*!40000 ALTER TABLE `ezpage_map_zones_pages` DISABLE KEYS */;
INSERT INTO `ezpage_map_zones_pages` VALUES (1,1),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(16,16),(17,17),(18,18),(19,19),(20,20),(21,21),(22,22),(23,23),(24,24),(25,25),(26,26),(27,27),(28,28),(29,29),(30,30),(31,31),(32,32),(33,33),(34,34),(35,35),(36,36),(37,37),(38,38),(39,39),(40,40),(41,41),(42,42),(43,43),(44,44),(49,49),(50,50),(55,55),(56,56),(57,57),(58,58),(59,59),(60,60),(61,61),(62,62),(63,63),(64,64),(65,65),(66,66),(67,67),(68,68),(69,69),(70,70),(71,71),(72,72),(73,73),(74,74),(75,75),(76,76),(77,77),(78,78),(79,79),(80,80),(81,81),(82,82),(83,83),(84,84),(85,85),(86,86),(87,87),(88,88),(89,89),(90,90),(91,91),(94,94),(181,181),(184,184),(185,185),(187,187),(398,398),(405,405),(426,426),(492,492),(493,493),(494,494),(495,495),(496,496),(498,498),(500,500),(553,553),(671,670),(672,670),(673,670),(674,670),(675,670),(676,670),(677,670),(678,670),(680,671),(681,671),(682,671),(683,671),(684,671),(685,671),(686,671),(687,671),(689,672),(690,672),(691,672),(692,672),(693,672),(694,672),(695,672),(696,672),(698,673),(699,673),(700,673),(701,673),(702,673),(703,673),(704,673),(705,673),(707,674),(708,674),(709,674),(710,674),(711,674),(712,674),(713,674),(714,674),(717,676),(718,676),(719,676),(720,676),(721,676),(722,676),(723,676),(724,676),(726,677),(727,677),(728,677),(729,677),(730,677),(731,677),(732,677),(733,677),(735,678),(736,678),(737,678),(738,678),(739,678),(740,678),(741,678),(742,678),(744,679),(745,679),(746,679),(747,679),(748,679),(749,679),(750,679),(751,679),(753,680),(754,680),(755,680),(756,680),(757,680),(758,680),(759,680),(760,680),(762,681),(763,681),(764,681),(765,681),(766,681),(767,681),(768,681),(769,681),(771,682),(772,682),(773,682),(774,682),(775,682),(776,682),(777,682),(778,682),(780,683),(781,683),(782,683),(783,683),(784,683),(785,683),(786,683),(787,683);
/*!40000 ALTER TABLE `ezpage_map_zones_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_pages`
--

DROP TABLE IF EXISTS `ezpage_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version_no` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `language_code` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `layout` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezpage_pages_content_id_version_no` (`content_id`,`version_no`)
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_pages`
--

LOCK TABLES `ezpage_pages` WRITE;
/*!40000 ALTER TABLE `ezpage_pages` DISABLE KEYS */;
INSERT INTO `ezpage_pages` VALUES (1,1,52,'eng-GB','default'),(398,13,57,'eng-GB','default'),(405,14,57,'eng-GB','default'),(426,15,57,'eng-GB','default'),(496,1,63,'eng-GB','default'),(498,16,57,'eng-GB','default'),(500,17,57,'eng-GB','default'),(553,18,57,'eng-GB','default');
/*!40000 ALTER TABLE `ezpage_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpage_zones`
--

DROP TABLE IF EXISTS `ezpage_zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpage_zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=788 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpage_zones`
--

LOCK TABLES `ezpage_zones` WRITE;
/*!40000 ALTER TABLE `ezpage_zones` DISABLE KEYS */;
INSERT INTO `ezpage_zones` VALUES (1,'default'),(4,'default'),(5,'default'),(6,'default'),(7,'default'),(8,'default'),(9,'default'),(10,'default'),(11,'default'),(16,'default'),(17,'default'),(18,'default'),(19,'default'),(20,'default'),(21,'default'),(22,'default'),(23,'default'),(24,'default'),(25,'default'),(26,'default'),(27,'default'),(28,'default'),(29,'default'),(30,'default'),(31,'default'),(32,'default'),(33,'default'),(34,'default'),(35,'default'),(36,'default'),(37,'default'),(38,'default'),(39,'default'),(40,'default'),(41,'default'),(42,'default'),(43,'default'),(44,'default'),(49,'default'),(50,'default'),(55,'default'),(56,'default'),(57,'default'),(58,'default'),(59,'default'),(60,'default'),(61,'default'),(62,'default'),(63,'default'),(64,'default'),(65,'default'),(66,'default'),(67,'default'),(68,'default'),(69,'default'),(70,'default'),(71,'default'),(72,'default'),(73,'default'),(74,'default'),(75,'default'),(76,'default'),(77,'default'),(78,'default'),(79,'default'),(80,'default'),(81,'default'),(82,'default'),(83,'default'),(84,'default'),(85,'default'),(86,'default'),(87,'default'),(88,'default'),(89,'default'),(90,'default'),(91,'default'),(94,'default'),(181,'default'),(184,'default'),(185,'default'),(187,'default'),(398,'default'),(405,'default'),(426,'default'),(492,'default'),(493,'default'),(494,'default'),(495,'default'),(496,'default'),(498,'default'),(500,'default'),(553,'default'),(671,'Top-Left Zone'),(672,'Top-Right Zone'),(673,'Left Zone'),(674,'Center Zone'),(675,'Rigth Zone'),(676,'Bottom-Left Zone'),(677,'Bottom-Right Zone'),(678,'Bottom Zone'),(680,'Top-Left Zone'),(681,'Top-Right Zone'),(682,'Left Zone'),(683,'Center Zone'),(684,'Rigth Zone'),(685,'Bottom-Left Zone'),(686,'Bottom-Right Zone'),(687,'Bottom Zone'),(689,'Top-Left Zone'),(690,'Top-Right Zone'),(691,'Left Zone'),(692,'Center Zone'),(693,'Rigth Zone'),(694,'Bottom-Left Zone'),(695,'Bottom-Right Zone'),(696,'Bottom Zone'),(698,'Top-Left Zone'),(699,'Top-Right Zone'),(700,'Left Zone'),(701,'Center Zone'),(702,'Rigth Zone'),(703,'Bottom-Left Zone'),(704,'Bottom-Right Zone'),(705,'Bottom Zone'),(707,'Top-Left Zone'),(708,'Top-Right Zone'),(709,'Left Zone'),(710,'Center Zone'),(711,'Rigth Zone'),(712,'Bottom-Left Zone'),(713,'Bottom-Right Zone'),(714,'Bottom Zone'),(717,'Top-Left Zone'),(718,'Top-Right Zone'),(719,'Left Zone'),(720,'Center Zone'),(721,'Rigth Zone'),(722,'Bottom-Left Zone'),(723,'Bottom-Right Zone'),(724,'Bottom Zone'),(726,'Top-Left Zone'),(727,'Top-Right Zone'),(728,'Left Zone'),(729,'Center Zone'),(730,'Rigth Zone'),(731,'Bottom-Left Zone'),(732,'Bottom-Right Zone'),(733,'Bottom Zone'),(735,'Top-Left Zone'),(736,'Top-Right Zone'),(737,'Left Zone'),(738,'Center Zone'),(739,'Rigth Zone'),(740,'Bottom-Left Zone'),(741,'Bottom-Right Zone'),(742,'Bottom Zone'),(744,'Top-Left Zone'),(745,'Top-Right Zone'),(746,'Left Zone'),(747,'Center Zone'),(748,'Rigth Zone'),(749,'Bottom-Left Zone'),(750,'Bottom-Right Zone'),(751,'Bottom Zone'),(753,'Top-Left Zone'),(754,'Top-Right Zone'),(755,'Left Zone'),(756,'Center Zone'),(757,'Rigth Zone'),(758,'Bottom-Left Zone'),(759,'Bottom-Right Zone'),(760,'Bottom Zone'),(762,'Top-Left Zone'),(763,'Top-Right Zone'),(764,'Left Zone'),(765,'Center Zone'),(766,'Rigth Zone'),(767,'Bottom-Left Zone'),(768,'Bottom-Right Zone'),(769,'Bottom Zone'),(771,'Top-Left Zone'),(772,'Top-Right Zone'),(773,'Left Zone'),(774,'Center Zone'),(775,'Rigth Zone'),(776,'Bottom-Left Zone'),(777,'Bottom-Right Zone'),(778,'Bottom Zone'),(780,'Top-Left Zone'),(781,'Top-Right Zone'),(782,'Left Zone'),(783,'Center Zone'),(784,'Rigth Zone'),(785,'Bottom-Left Zone'),(786,'Bottom-Right Zone'),(787,'Bottom Zone');
/*!40000 ALTER TABLE `ezpage_zones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy`
--

DROP TABLE IF EXISTS `ezpolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `module_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `original_id` int(11) NOT NULL DEFAULT 0,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpolicy_role_id` (`role_id`),
  KEY `ezpolicy_original_id` (`original_id`)
) ENGINE=InnoDB AUTO_INCREMENT=344 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy`
--

LOCK TABLES `ezpolicy` WRITE;
/*!40000 ALTER TABLE `ezpolicy` DISABLE KEYS */;
INSERT INTO `ezpolicy` VALUES (317,'*','content',0,3),(319,'login','user',0,3),(332,'*','*',0,2),(333,'read','content',0,4),(340,'*','url',0,3),(341,'read','content',0,1),(342,'login','user',0,1),(343,'view_embed','content',0,1);
/*!40000 ALTER TABLE `ezpolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy_limitation`
--

DROP TABLE IF EXISTS `ezpolicy_limitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy_limitation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `policy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `policy_id` (`policy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy_limitation`
--

LOCK TABLES `ezpolicy_limitation` WRITE;
/*!40000 ALTER TABLE `ezpolicy_limitation` DISABLE KEYS */;
INSERT INTO `ezpolicy_limitation` VALUES (252,'Section',329),(254,'Class',333),(255,'Owner',333),(257,'Section',341),(259,'Class',343),(260,'SiteAccess',342);
/*!40000 ALTER TABLE `ezpolicy_limitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy_limitation_value`
--

DROP TABLE IF EXISTS `ezpolicy_limitation_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy_limitation_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limitation_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpolicy_limit_value_limit_id` (`limitation_id`),
  KEY `ezpolicy_limitation_value_val` (`value`(191))
) ENGINE=InnoDB AUTO_INCREMENT=493 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy_limitation_value`
--

LOCK TABLES `ezpolicy_limitation_value` WRITE;
/*!40000 ALTER TABLE `ezpolicy_limitation_value` DISABLE KEYS */;
INSERT INTO `ezpolicy_limitation_value` VALUES (478,252,'1'),(480,254,'4'),(481,255,'1'),(486,257,'1'),(487,257,'3'),(488,257,'6'),(490,259,'5'),(491,259,'12'),(492,260,'28834503');
/*!40000 ALTER TABLE `ezpolicy_limitation_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpreferences`
--

DROP TABLE IF EXISTS `ezpreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpreferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpreferences_user_id_idx` (`user_id`,`name`),
  KEY `ezpreferences_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpreferences`
--

LOCK TABLES `ezpreferences` WRITE;
/*!40000 ALTER TABLE `ezpreferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrole`
--

DROP TABLE IF EXISTS `ezrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_new` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `value` char(1) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `version` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrole`
--

LOCK TABLES `ezrole` WRITE;
/*!40000 ALTER TABLE `ezrole` DISABLE KEYS */;
INSERT INTO `ezrole` VALUES (1,0,'Anonymous','0',0),(2,0,'Administrator','0',0),(3,0,'Editor','',0),(4,0,'Member','',0);
/*!40000 ALTER TABLE `ezrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsearch_object_word_link`
--

DROP TABLE IF EXISTS `ezsearch_object_word_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsearch_object_word_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT 0,
  `contentclass_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `frequency` double NOT NULL DEFAULT 0,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `integer_value` int(11) NOT NULL DEFAULT 0,
  `next_word_id` int(11) NOT NULL DEFAULT 0,
  `placement` int(11) NOT NULL DEFAULT 0,
  `prev_word_id` int(11) NOT NULL DEFAULT 0,
  `published` int(11) NOT NULL DEFAULT 0,
  `section_id` int(11) NOT NULL DEFAULT 0,
  `word_id` int(11) NOT NULL DEFAULT 0,
  `language_mask` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `ezsearch_object_word_link_object` (`contentobject_id`),
  KEY `ezsearch_object_word_link_identifier` (`identifier`(191)),
  KEY `ezsearch_object_word_link_integer_value` (`integer_value`),
  KEY `ezsearch_object_word_link_word` (`word_id`),
  KEY `ezsearch_object_word_link_frequency` (`frequency`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsearch_object_word_link`
--

LOCK TABLES `ezsearch_object_word_link` WRITE;
/*!40000 ALTER TABLE `ezsearch_object_word_link` DISABLE KEYS */;
INSERT INTO `ezsearch_object_word_link` VALUES (46,7,3,4,0,'description',0,35,0,0,1033917596,2,34,3),(47,7,3,4,0,'description',0,36,1,34,1033917596,2,35,3),(48,6,3,4,0,'name',0,0,2,35,1033917596,2,36,3),(49,8,4,10,0,'first_name',0,38,0,0,1033920665,2,37,3),(50,9,4,10,0,'last_name',0,0,1,37,1033920665,2,38,3),(51,6,3,11,0,'name',0,40,0,0,1033920746,2,39,3),(52,6,3,11,0,'name',0,0,1,39,1033920746,2,40,3),(53,6,3,12,0,'name',0,36,0,0,1033920775,2,41,3),(54,6,3,12,0,'name',0,0,1,41,1033920775,2,36,3),(55,6,3,13,0,'name',0,0,0,0,1033920794,2,42,3),(56,8,4,14,0,'first_name',0,38,0,0,1033920830,2,41,3),(57,9,4,14,0,'last_name',0,0,1,41,1033920830,2,38,3),(58,4,1,41,0,'name',0,0,0,0,1060695457,3,43,3),(59,6,3,42,0,'name',0,36,0,0,1072180330,2,37,3),(60,6,3,42,0,'name',0,38,1,37,1072180330,2,36,3),(61,7,3,42,0,'description',0,35,2,36,1072180330,2,38,3),(62,7,3,42,0,'description',0,44,3,38,1072180330,2,35,3),(63,7,3,42,0,'description',0,13,4,35,1072180330,2,44,3),(64,7,3,42,0,'description',0,37,5,44,1072180330,2,13,3),(65,7,3,42,0,'description',0,38,6,13,1072180330,2,37,3),(66,7,3,42,0,'description',0,0,7,37,1072180330,2,38,3),(67,4,1,49,0,'name',0,0,0,0,1080220197,3,45,3),(68,4,1,50,0,'name',0,0,0,0,1080220220,3,46,3),(69,4,1,51,0,'name',0,0,0,0,1080220233,3,47,3),(70,185,42,52,0,'name',0,48,0,0,1442481743,1,1,3),(71,185,42,52,0,'name',0,49,1,1,1442481743,1,48,3),(72,185,42,52,0,'name',0,2,2,48,1442481743,1,49,3),(73,185,42,52,0,'name',0,3,3,49,1442481743,1,2,3),(74,186,42,52,0,'description',0,4,4,2,1442481743,1,3,3),(75,186,42,52,0,'description',0,5,5,3,1442481743,1,4,3),(76,186,42,52,0,'description',0,6,6,4,1442481743,1,5,3),(77,186,42,52,0,'description',0,7,7,5,1442481743,1,6,3),(78,186,42,52,0,'description',0,8,8,6,1442481743,1,7,3),(79,186,42,52,0,'description',0,9,9,7,1442481743,1,8,3),(80,186,42,52,0,'description',0,10,10,8,1442481743,1,9,3),(81,186,42,52,0,'description',0,0,11,9,1442481743,1,10,3),(82,4,1,53,0,'name',0,51,0,0,1486473151,3,50,3),(83,4,1,53,0,'name',0,50,1,50,1486473151,3,51,3),(84,155,1,53,0,'short_name',0,51,2,51,1486473151,3,50,3),(85,155,1,53,0,'short_name',0,52,3,50,1486473151,3,51,3),(86,119,1,53,0,'short_description',0,44,4,51,1486473151,3,52,3),(87,119,1,53,0,'short_description',0,53,5,52,1486473151,3,44,3),(88,119,1,53,0,'short_description',0,51,6,44,1486473151,3,53,3),(89,119,1,53,0,'short_description',0,0,7,53,1486473151,3,51,3),(90,4,1,54,0,'name',0,0,0,0,1537166893,6,54,2),(91,4,1,55,0,'name',0,56,0,0,1586855342,7,55,3),(92,4,1,55,0,'name',0,0,1,55,1586855342,7,56,3),(123,116,5,60,0,'name',0,0,0,0,1623796085,3,87,3),(128,116,5,64,0,'name',0,93,0,0,1624034225,3,92,3),(129,116,5,64,0,'name',0,0,1,92,1624034225,3,93,3),(130,195,45,63,0,'title',0,94,0,0,1624034263,1,87,3),(131,195,45,63,0,'title',0,95,1,87,1624034263,1,94,3),(132,195,45,63,0,'title',0,96,2,94,1624034263,1,95,3),(133,195,45,63,0,'title',0,97,3,95,1624034263,1,96,3),(134,197,45,63,0,'promo_title',0,87,4,96,1624034263,1,97,3),(135,197,45,63,0,'promo_title',0,98,5,97,1624034263,1,87,3),(136,199,45,63,0,'promo_description',0,0,6,87,1624034263,1,98,3),(141,4,1,65,0,'name',0,104,0,0,1624244274,3,103,3),(142,4,1,65,0,'name',0,0,1,103,1624244274,3,104,3),(143,4,1,66,0,'name',0,45,0,0,1624244309,3,105,3),(144,4,1,66,0,'name',0,0,1,105,1624244309,3,45,3),(191,116,5,70,0,'name',0,93,0,0,1624245926,3,92,3),(192,116,5,70,0,'name',0,0,1,92,1624245926,3,93,3),(193,116,5,72,0,'name',0,145,0,0,1624248872,3,144,3),(194,116,5,72,0,'name',0,146,1,144,1624248872,3,145,3),(195,116,5,72,0,'name',0,145,2,145,1624248872,3,146,3),(196,116,5,72,0,'name',0,147,3,146,1624248872,3,145,3),(197,116,5,72,0,'name',0,148,4,145,1624248872,3,147,3),(198,116,5,72,0,'name',0,149,5,147,1624248872,3,148,3),(199,116,5,72,0,'name',0,150,6,148,1624248872,3,149,3),(200,116,5,72,0,'name',0,151,7,149,1624248872,3,150,3),(201,116,5,72,0,'name',0,152,8,150,1624248872,3,151,3),(202,116,5,72,0,'name',0,93,9,151,1624248872,3,152,3),(203,116,5,72,0,'name',0,0,10,152,1624248872,3,93,3),(226,116,5,74,0,'name',0,93,0,0,1624249203,3,92,3),(227,116,5,74,0,'name',0,0,1,92,1624249203,3,93,3),(228,200,46,73,0,'title',0,174,0,0,1624249205,3,173,3),(229,200,46,73,0,'title',0,175,1,173,1624249205,3,174,3),(230,200,46,73,0,'title',0,176,2,174,1624249205,3,175,3),(231,204,46,73,0,'description',0,167,3,175,1624249205,3,176,3),(232,204,46,73,0,'description',0,177,4,176,1624249205,3,167,3),(233,204,46,73,0,'description',0,178,5,167,1624249205,3,177,3),(234,204,46,73,0,'description',0,179,6,177,1624249205,3,178,3),(235,204,46,73,0,'description',0,157,7,178,1624249205,3,179,3),(236,204,46,73,0,'description',0,180,8,179,1624249205,3,157,3),(237,204,46,73,0,'description',0,162,9,157,1624249205,3,180,3),(238,204,46,73,0,'description',0,181,10,180,1624249205,3,162,3),(239,204,46,73,0,'description',0,182,11,162,1624249205,3,181,3),(240,204,46,73,0,'description',0,169,12,181,1624249205,3,182,3),(241,204,46,73,0,'description',0,183,13,182,1624249205,3,169,3),(242,204,46,73,0,'description',0,157,14,169,1624249205,3,183,3),(243,204,46,73,0,'description',0,184,15,183,1624249205,3,157,3),(244,204,46,73,0,'description',0,176,16,157,1624249205,3,184,3),(245,204,46,73,0,'description',0,185,17,184,1624249205,3,176,3),(246,204,46,73,0,'description',0,186,18,176,1624249205,3,185,3),(247,204,46,73,0,'description',0,187,19,185,1624249205,3,186,3),(248,204,46,73,0,'description',0,171,20,186,1624249205,3,187,3),(249,202,46,73,0,'cta_label',0,172,21,187,1624249205,3,171,3),(250,202,46,73,0,'cta_label',0,188,22,171,1624249205,3,172,3),(251,205,46,73,0,'cta_external_link',0,189,23,172,1624249205,3,188,3),(252,205,46,73,0,'cta_external_link',0,190,24,188,1624249205,3,189,3),(253,205,46,73,0,'cta_external_link',0,191,25,189,1624249205,3,190,3),(254,205,46,73,0,'cta_external_link',0,0,26,190,1624249205,3,191,3),(255,190,44,57,0,'title',0,193,0,0,1622836809,1,192,3),(256,190,44,57,0,'title',0,0,1,192,1622836809,1,193,3),(257,116,5,75,0,'name',0,0,0,0,1624311259,3,194,3),(258,200,46,71,0,'title',0,94,0,0,1624248941,3,87,3),(259,200,46,71,0,'title',0,195,1,87,1624248941,3,94,3),(260,200,46,71,0,'title',0,196,2,94,1624248941,3,195,3),(261,204,46,71,0,'description',0,197,3,195,1624248941,3,196,3),(262,204,46,71,0,'description',0,198,4,196,1624248941,3,197,3),(263,204,46,71,0,'description',0,157,5,197,1624248941,3,198,3),(264,204,46,71,0,'description',0,199,6,198,1624248941,3,157,3),(265,204,46,71,0,'description',0,200,7,157,1624248941,3,199,3),(266,204,46,71,0,'description',0,201,8,199,1624248941,3,200,3),(267,204,46,71,0,'description',0,202,9,200,1624248941,3,201,3),(268,204,46,71,0,'description',0,162,10,201,1624248941,3,202,3),(269,204,46,71,0,'description',0,203,11,202,1624248941,3,162,3),(270,204,46,71,0,'description',0,204,12,162,1624248941,3,203,3),(271,204,46,71,0,'description',0,205,13,203,1624248941,3,204,3),(272,204,46,71,0,'description',0,206,14,204,1624248941,3,205,3),(273,204,46,71,0,'description',0,167,15,205,1624248941,3,206,3),(274,204,46,71,0,'description',0,207,16,206,1624248941,3,167,3),(275,204,46,71,0,'description',0,169,17,167,1624248941,3,207,3),(276,204,46,71,0,'description',0,208,18,207,1624248941,3,169,3),(277,204,46,71,0,'description',0,171,19,169,1624248941,3,208,3),(278,202,46,71,0,'cta_label',0,172,20,208,1624248941,3,171,3),(279,202,46,71,0,'cta_label',0,0,21,171,1624248941,3,172,3),(282,116,5,59,0,'name',0,96,0,0,1622948256,3,95,3),(283,116,5,59,0,'name',0,195,1,95,1622948256,3,96,3),(284,116,5,59,0,'name',0,95,2,96,1622948256,3,195,3),(285,117,5,59,0,'caption',0,96,3,195,1622948256,3,95,3),(286,117,5,59,0,'caption',0,195,4,95,1622948256,3,96,3),(287,117,5,59,0,'caption',0,0,5,96,1622948256,3,195,3);
/*!40000 ALTER TABLE `ezsearch_object_word_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsearch_word`
--

DROP TABLE IF EXISTS `ezsearch_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsearch_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_count` int(11) NOT NULL DEFAULT 0,
  `word` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezsearch_word_word_i` (`word`),
  KEY `ezsearch_word_obj_count` (`object_count`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsearch_word`
--

LOCK TABLES `ezsearch_word` WRITE;
/*!40000 ALTER TABLE `ezsearch_word` DISABLE KEYS */;
INSERT INTO `ezsearch_word` VALUES (1,1,'ibexa'),(2,1,'platform'),(3,1,'you'),(4,1,'are'),(5,1,'now'),(6,1,'ready'),(7,1,'to'),(8,1,'start'),(9,1,'your'),(10,1,'project'),(13,1,'the'),(34,1,'main'),(35,2,'group'),(36,3,'users'),(37,2,'anonymous'),(38,3,'user'),(39,1,'guest'),(40,1,'accounts'),(41,2,'administrator'),(42,1,'editors'),(43,1,'media'),(44,2,'for'),(45,2,'images'),(46,1,'files'),(47,1,'multimedia'),(48,1,'digital'),(49,1,'experience'),(50,1,'form'),(51,1,'uploads'),(52,1,'folder'),(53,1,'file'),(54,1,'forms'),(55,1,'site'),(56,1,'skeletons'),(87,3,'lorem'),(92,3,'basura'),(93,4,'png'),(94,2,'ipsum'),(95,2,'sit'),(96,2,'amet'),(97,1,'promo'),(98,1,'svdsvdsvdsv'),(103,1,'test'),(104,1,'content'),(105,1,'hero'),(144,1,'captura'),(145,1,'de'),(146,1,'pantalla'),(147,1,'2021'),(148,1,'06'),(149,1,'18'),(150,1,'09'),(151,1,'59'),(152,1,'26'),(157,2,'aliquam'),(162,2,'sed'),(167,2,'commodo'),(169,2,'vel'),(171,2,'see'),(172,2,'more'),(173,1,'integer'),(174,1,'facilisis'),(175,1,'semper'),(176,1,'nullam'),(177,1,'massa'),(178,1,'vitae'),(179,1,'nibh'),(180,1,'auctor'),(181,1,'a'),(182,1,'lectus'),(183,1,'mauris'),(184,1,'tincidunt'),(185,1,'mattis'),(186,1,'ex'),(187,1,'felis'),(188,1,'https'),(189,1,'www'),(190,1,'google'),(191,1,'com'),(192,1,'axp'),(193,1,'home'),(194,1,'hero1'),(195,2,'dolor'),(196,1,'consectetur'),(197,1,'adipiscing'),(198,1,'elit'),(199,1,'pulvinar'),(200,1,'faucibus'),(201,1,'sollicitudin'),(202,1,'nulla'),(203,1,'orci'),(204,1,'ac'),(205,1,'libero'),(206,1,'eleifend'),(207,1,'quis'),(208,1,'justo');
/*!40000 ALTER TABLE `ezsearch_word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsection`
--

DROP TABLE IF EXISTS `ezsection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `navigation_part_identifier` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'ezcontentnavigationpart',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsection`
--

LOCK TABLES `ezsection` WRITE;
/*!40000 ALTER TABLE `ezsection` DISABLE KEYS */;
INSERT INTO `ezsection` VALUES (1,'standard','','Standard','ezcontentnavigationpart'),(2,'users','','Users','ezusernavigationpart'),(3,'media','','Media','ezmedianavigationpart'),(6,'form',NULL,'Form','ezcontentnavigationpart'),(7,'site_skeleton',NULL,'Site skeleton','ezcontentnavigationpart');
/*!40000 ALTER TABLE `ezsection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsite`
--

DROP TABLE IF EXISTS `ezsite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsite`
--

LOCK TABLES `ezsite` WRITE;
/*!40000 ALTER TABLE `ezsite` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezsite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsite_data`
--

DROP TABLE IF EXISTS `ezsite_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsite_data` (
  `name` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsite_data`
--

LOCK TABLES `ezsite_data` WRITE;
/*!40000 ALTER TABLE `ezsite_data` DISABLE KEYS */;
INSERT INTO `ezsite_data` VALUES ('ezplatform-release','3.0.0');
/*!40000 ALTER TABLE `ezsite_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsite_public_access`
--

DROP TABLE IF EXISTS `ezsite_public_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsite_public_access` (
  `public_access_identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `site_id` int(11) NOT NULL,
  `site_access_group` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `config` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `site_matcher_host` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `site_matcher_path` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`public_access_identifier`),
  KEY `ezsite_public_access_site_id` (`site_id`),
  CONSTRAINT `fk_ezsite_public_access_site_id` FOREIGN KEY (`site_id`) REFERENCES `ezsite` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsite_public_access`
--

LOCK TABLES `ezsite_public_access` WRITE;
/*!40000 ALTER TABLE `ezsite_public_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezsite_public_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurl`
--

DROP TABLE IF EXISTS `ezurl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NOT NULL DEFAULT 0,
  `is_valid` int(11) NOT NULL DEFAULT 1,
  `last_checked` int(11) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0,
  `original_url_md5` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `url` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezurl_url` (`url`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurl`
--

LOCK TABLES `ezurl` WRITE;
/*!40000 ALTER TABLE `ezurl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezurl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurl_object_link`
--

DROP TABLE IF EXISTS `ezurl_object_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurl_object_link` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT 0,
  `contentobject_attribute_version` int(11) NOT NULL DEFAULT 0,
  `url_id` int(11) NOT NULL DEFAULT 0,
  KEY `ezurl_ol_coa_id` (`contentobject_attribute_id`),
  KEY `ezurl_ol_url_id` (`url_id`),
  KEY `ezurl_ol_coa_version` (`contentobject_attribute_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurl_object_link`
--

LOCK TABLES `ezurl_object_link` WRITE;
/*!40000 ALTER TABLE `ezurl_object_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezurl_object_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias`
--

DROP TABLE IF EXISTS `ezurlalias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `forward_to_id` int(11) NOT NULL DEFAULT 0,
  `is_imported` int(11) NOT NULL DEFAULT 0,
  `is_internal` int(11) NOT NULL DEFAULT 1,
  `is_wildcard` int(11) NOT NULL DEFAULT 0,
  `source_md5` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ezurlalias_source_md5` (`source_md5`),
  KEY `ezurlalias_wcard_fwd` (`is_wildcard`,`forward_to_id`),
  KEY `ezurlalias_forward_to_id` (`forward_to_id`),
  KEY `ezurlalias_imp_wcard_fwd` (`is_imported`,`is_wildcard`,`forward_to_id`),
  KEY `ezurlalias_source_url` (`source_url`(191)),
  KEY `ezurlalias_desturl` (`destination_url`(191))
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias`
--

LOCK TABLES `ezurlalias` WRITE;
/*!40000 ALTER TABLE `ezurlalias` DISABLE KEYS */;
INSERT INTO `ezurlalias` VALUES (12,'content/view/full/2',0,1,1,0,'d41d8cd98f00b204e9800998ecf8427e',''),(13,'content/view/full/5',0,1,1,0,'9bc65c2abec141778ffaa729489f3e87','users'),(15,'content/view/full/12',0,1,1,0,'02d4e844e3a660857a3f81585995ffe1','users/guest_accounts'),(16,'content/view/full/13',0,1,1,0,'1b1d79c16700fd6003ea7be233e754ba','users/administrator_users'),(17,'content/view/full/14',0,1,1,0,'0bb9dd665c96bbc1cf36b79180786dea','users/editors'),(18,'content/view/full/15',0,1,1,0,'f1305ac5f327a19b451d82719e0c3f5d','users/administrator_users/administrator_user'),(20,'content/view/full/43',0,1,1,0,'62933a2951ef01f4eafd9bdf4d3cd2f0','media'),(21,'content/view/full/44',0,1,1,0,'3ae1aac958e1c82013689d917d34967a','users/anonymous_users'),(22,'content/view/full/45',0,1,1,0,'aad93975f09371695ba08292fd9698db','users/anonymous_users/anonymous_user'),(28,'content/view/full/51',0,1,1,0,'38985339d4a5aadfc41ab292b4527046','media/images'),(29,'content/view/full/52',0,1,1,0,'ad5a8c6f6aac3b1b9df267fe22e7aef6','media/files'),(30,'content/view/full/53',0,1,1,0,'562a0ac498571c6c3529173184a2657c','media/multimedia');
/*!40000 ALTER TABLE `ezurlalias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias_ml`
--

DROP TABLE IF EXISTS `ezurlalias_ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias_ml` (
  `parent` int(11) NOT NULL DEFAULT 0,
  `text_md5` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `action` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `action_type` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `alias_redirects` int(11) NOT NULL DEFAULT 1,
  `id` int(11) NOT NULL DEFAULT 0,
  `is_alias` int(11) NOT NULL DEFAULT 0,
  `is_original` int(11) NOT NULL DEFAULT 0,
  `lang_mask` bigint(20) NOT NULL DEFAULT 0,
  `link` int(11) NOT NULL DEFAULT 0,
  `text` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`parent`,`text_md5`),
  KEY `ezurlalias_ml_actt_org_al` (`action_type`,`is_original`,`is_alias`),
  KEY `ezurlalias_ml_text_lang` (`text`(32),`lang_mask`,`parent`),
  KEY `ezurlalias_ml_par_act_id_lnk` (`action`(32),`id`,`link`,`parent`),
  KEY `ezurlalias_ml_par_lnk_txt` (`parent`,`text`(32),`link`),
  KEY `ezurlalias_ml_act_org` (`action`(32),`is_original`),
  KEY `ezurlalias_ml_text` (`text`(32),`id`,`link`),
  KEY `ezurlalias_ml_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias_ml`
--

LOCK TABLES `ezurlalias_ml` WRITE;
/*!40000 ALTER TABLE `ezurlalias_ml` DISABLE KEYS */;
INSERT INTO `ezurlalias_ml` VALUES (0,'25d9c27d68ba0e40468e05a61f96d31d','eznode:56','eznode',0,41,0,1,3,41,'site-skeletons'),(0,'50e2736330de124f6edea9b008556fe6','nop:','nop',1,17,0,0,1,17,'media2'),(0,'62933a2951ef01f4eafd9bdf4d3cd2f0','eznode:43','eznode',1,9,0,1,3,9,'Media'),(0,'802e2c30772a7d8b4233779141f8083d','eznode:57','eznode',1,42,0,1,3,42,'axp-home'),(0,'86425c35a33507d479f71ade53a669aa','nop:','nop',1,3,0,0,1,3,'users2'),(0,'9bc65c2abec141778ffaa729489f3e87','eznode:5','eznode',1,2,0,1,3,2,'Users'),(0,'ac68b62abfd6a9fe26e8ac4236c8ce0c','eznode:55','eznode',0,39,0,1,2,39,'forms'),(0,'d41d8cd98f00b204e9800998ecf8427e','eznode:2','eznode',1,1,0,1,3,1,''),(2,'a147e136bfa717592f2bd70bd4b53b17','eznode:14','eznode',1,6,0,1,3,6,'Editors'),(2,'c2803c3fa1b0b5423237b4e018cae755','eznode:44','eznode',1,10,0,1,3,10,'Anonymous-Users'),(2,'e57843d836e3af8ab611fde9e2139b3a','eznode:12','eznode',1,4,0,1,3,4,'Guest-accounts'),(2,'f89fad7f8a3abc8c09e1deb46a420007','eznode:13','eznode',1,5,0,1,3,5,'Administrator-users'),(3,'505e93077a6dde9034ad97a14ab022b1','nop:','nop',1,11,0,0,1,11,'anonymous_users2'),(3,'70bb992820e73638731aa8de79b3329e','eznode:12','eznode',1,26,0,0,1,4,'guest_accounts'),(3,'a147e136bfa717592f2bd70bd4b53b17','eznode:14','eznode',1,29,0,0,1,6,'editors'),(3,'a7da338c20bf65f9f789c87296379c2a','nop:','nop',1,7,0,0,1,7,'administrator_users2'),(3,'aeb8609aa933b0899aa012c71139c58c','eznode:13','eznode',1,27,0,0,1,5,'administrator_users'),(3,'e9e5ad0c05ee1a43715572e5cc545926','eznode:44','eznode',1,30,0,0,1,10,'anonymous_users'),(5,'5a9d7b0ec93173ef4fedee023209cb61','eznode:15','eznode',1,8,0,1,3,8,'Administrator-User'),(7,'a3cca2de936df1e2f805710399989971','eznode:15','eznode',1,28,0,0,0,8,'administrator_user'),(9,'2e5bc8831f7ae6a29530e7f1bbf2de9c','eznode:53','eznode',1,20,0,1,3,20,'Multimedia'),(9,'45b963397aa40d4a0063e0d85e4fe7a1','eznode:52','eznode',1,19,0,1,3,19,'Files'),(9,'59b514174bffe4ae402b3d63aad79fe0','eznode:51','eznode',1,18,0,1,3,18,'Images'),(9,'9749fad13d6e7092a6337c4af9d83764','eznode:62','eznode',0,47,0,1,3,47,'test-content'),(10,'ccb62ebca03a31272430bc414bd5cd5b','eznode:45','eznode',1,12,0,1,3,12,'Anonymous-User'),(11,'c593ec85293ecb0e02d50d4c5c6c20eb','eznode:45','eznode',1,31,0,0,1,12,'anonymous_user'),(17,'2e5bc8831f7ae6a29530e7f1bbf2de9c','eznode:53','eznode',1,34,0,0,1,20,'multimedia'),(17,'45b963397aa40d4a0063e0d85e4fe7a1','eznode:52','eznode',1,33,0,0,1,19,'files'),(17,'59b514174bffe4ae402b3d63aad79fe0','eznode:51','eznode',1,32,0,0,1,18,'images'),(18,'0cc07ebc29d423c07e5191089bd9f4c8','eznode:60','eznode',0,45,0,1,3,45,'basura.png'),(18,'0d047e1b4604aeabdf77adc066011ea7','eznode:67','eznode',0,52,0,1,3,52,'basura.png2'),(18,'11c4dda5b89bb5e8bb6ce1a2177b7966','eznode:70','eznode',0,55,0,1,3,55,'basura.png3'),(18,'432a42379e4c3dc8a48fa25dd5fcf79a','eznode:58','eznode',1,58,0,0,3,43,'sfgdfg'),(18,'50d3c2b1f593652d12c0cccdda5561a9','eznode:68','eznode',0,53,0,1,3,53,'captura-de-pantalla-de-2021-06-18-09-59-26.png'),(18,'7b4b5018038c33a752ccd3b5b0ee7f61','eznode:72','eznode',0,57,0,1,3,57,'hero1'),(18,'86e4d8d2ea3ac40223a7092f07b594f7','eznode:58','eznode',0,43,0,1,3,43,'sit-amet-dolor'),(18,'d2e16e6ef52a45b7468f1da56bba1953','eznode:59','eznode',0,44,0,1,3,44,'lorem'),(19,'2c5f0c4eb6b8ba8d176b87665bdbe1af','eznode:54','eznode',0,38,0,1,3,38,'form-uploads'),(42,'8a0c73794da4284dd2f592fe75113a9c','eznode:61','eznode',0,46,0,1,3,46,'lorem-ipsum-sit-amet'),(47,'b44142644ebca39d969c951d8b2cbfab','eznode:63','eznode',0,48,0,1,3,48,'hero-images'),(48,'3d6e455e1a353107b5e4d2f37f898f35','eznode:69','eznode',1,54,0,1,3,54,'lorem-ipsum-dolor'),(48,'765da10a13db79a720ce45c43a78efe8','eznode:71','eznode',0,56,0,1,3,56,'integer-facilisis-semper');
/*!40000 ALTER TABLE `ezurlalias_ml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias_ml_incr`
--

DROP TABLE IF EXISTS `ezurlalias_ml_incr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias_ml_incr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias_ml_incr`
--

LOCK TABLES `ezurlalias_ml_incr` WRITE;
/*!40000 ALTER TABLE `ezurlalias_ml_incr` DISABLE KEYS */;
INSERT INTO `ezurlalias_ml_incr` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36),(37),(38),(39),(40),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56),(57),(58);
/*!40000 ALTER TABLE `ezurlalias_ml_incr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlwildcard`
--

DROP TABLE IF EXISTS `ezurlwildcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlwildcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlwildcard`
--

LOCK TABLES `ezurlwildcard` WRITE;
/*!40000 ALTER TABLE `ezurlwildcard` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezurlwildcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser`
--

DROP TABLE IF EXISTS `ezuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser` (
  `contentobject_id` int(11) NOT NULL DEFAULT 0,
  `email` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `password_hash_type` int(11) NOT NULL DEFAULT 1,
  `password_updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`contentobject_id`),
  UNIQUE KEY `ezuser_login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser`
--

LOCK TABLES `ezuser` WRITE;
/*!40000 ALTER TABLE `ezuser` DISABLE KEYS */;
INSERT INTO `ezuser` VALUES (10,'anonymous@link.invalid','anonymous','$2y$10$35gOSQs6JK4u4whyERaeUuVeQBi2TUBIZIfP7HEj7sfz.MxvTuOeC',7,NULL),(14,'admin@link.invalid','admin','$2y$10$FDn9NPwzhq85cLLxfD5Wu.L3SL3Z/LNCvhkltJUV0wcJj7ciJg2oy',7,NULL);
/*!40000 ALTER TABLE `ezuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_accountkey`
--

DROP TABLE IF EXISTS `ezuser_accountkey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_accountkey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash_key` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `hash_key` (`hash_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_accountkey`
--

LOCK TABLES `ezuser_accountkey` WRITE;
/*!40000 ALTER TABLE `ezuser_accountkey` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezuser_accountkey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_role`
--

DROP TABLE IF EXISTS `ezuser_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) DEFAULT NULL,
  `limit_identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `limit_value` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezuser_role_role_id` (`role_id`),
  KEY `ezuser_role_contentobject_id` (`contentobject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_role`
--

LOCK TABLES `ezuser_role` WRITE;
/*!40000 ALTER TABLE `ezuser_role` DISABLE KEYS */;
INSERT INTO `ezuser_role` VALUES (32,13,'Subtree','/1/2/',3),(33,13,'Subtree','/1/43/',3),(34,12,'','',2),(35,13,'','',4),(36,11,'','',1),(37,42,'','',1);
/*!40000 ALTER TABLE `ezuser_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_setting`
--

DROP TABLE IF EXISTS `ezuser_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_setting` (
  `user_id` int(11) NOT NULL DEFAULT 0,
  `is_enabled` int(11) NOT NULL DEFAULT 0,
  `max_login` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_setting`
--

LOCK TABLES `ezuser_setting` WRITE;
/*!40000 ALTER TABLE `ezuser_setting` DISABLE KEYS */;
INSERT INTO `ezuser_setting` VALUES (10,1,1000),(14,1,10);
/*!40000 ALTER TABLE `ezuser_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ibexa_migrations`
--

DROP TABLE IF EXISTS `ibexa_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ibexa_migrations` (
  `name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibexa_migrations`
--

LOCK TABLES `ibexa_migrations` WRITE;
/*!40000 ALTER TABLE `ibexa_migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ibexa_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ibexa_segment_group_map`
--

DROP TABLE IF EXISTS `ibexa_segment_group_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ibexa_segment_group_map` (
  `segment_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`segment_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibexa_segment_group_map`
--

LOCK TABLES `ibexa_segment_group_map` WRITE;
/*!40000 ALTER TABLE `ibexa_segment_group_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `ibexa_segment_group_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ibexa_segment_groups`
--

DROP TABLE IF EXISTS `ibexa_segment_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ibexa_segment_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`identifier`),
  UNIQUE KEY `ibexa_segment_groups_identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibexa_segment_groups`
--

LOCK TABLES `ibexa_segment_groups` WRITE;
/*!40000 ALTER TABLE `ibexa_segment_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `ibexa_segment_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ibexa_segment_user_map`
--

DROP TABLE IF EXISTS `ibexa_segment_user_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ibexa_segment_user_map` (
  `segment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`segment_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibexa_segment_user_map`
--

LOCK TABLES `ibexa_segment_user_map` WRITE;
/*!40000 ALTER TABLE `ibexa_segment_user_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `ibexa_segment_user_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ibexa_segments`
--

DROP TABLE IF EXISTS `ibexa_segments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ibexa_segments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`identifier`),
  UNIQUE KEY `ibexa_segments_identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibexa_segments`
--

LOCK TABLES `ibexa_segments` WRITE;
/*!40000 ALTER TABLE `ibexa_segments` DISABLE KEYS */;
/*!40000 ALTER TABLE `ibexa_segments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ibexa_setting`
--

DROP TABLE IF EXISTS `ibexa_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ibexa_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL COMMENT '(DC2Type:json)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ibexa_setting_group_identifier` (`group`,`identifier`),
  KEY `ibexa_setting_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ibexa_setting`
--

LOCK TABLES `ibexa_setting` WRITE;
/*!40000 ALTER TABLE `ibexa_setting` DISABLE KEYS */;
INSERT INTO `ibexa_setting` VALUES (1,'personalization','installation_key','\"\"');
/*!40000 ALTER TABLE `ibexa_setting` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-11  5:44:03
