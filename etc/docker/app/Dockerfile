##### Building Base image #####
FROM php:7.4.19-fpm-alpine as base

RUN apk --update --no-cache add acl supervisor libpng-dev libgomp icu icu-dev libxslt libxslt-dev imagemagick imagemagick-dev autoconf nginx g++ make git jpegoptim libzip libzip-dev curl-dev openssh nodejs yarn && \
    docker-php-ext-install pdo pdo_mysql mysqli opcache gd intl xml xsl zip curl && \
    pecl install imagick redis python igbinary && \
    docker-php-ext-enable imagick redis igbinary && \
    apk del --purge libpng-dev icu-dev libxslt-dev imagemagick-dev autoconf g++ make libzip-dev curl-dev && \
    mkdir -m 0755 -p var && \
    chown -Rf www-data:www-data var 

ARG COMPOSER_VERSION=2.0.14
RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/bin --version=$COMPOSER_VERSION --filename=composer

WORKDIR /app

ARG COMPOSER_AUTH_EZ_USERNAME
ARG COMPOSER_AUTH_EZ_PASSWORD
ARG GITLAB_ACCESS_TOKEN

ENV APP_ENV=prod
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_CACHE_DIR=/dev/null
ENV COMPOSER_MEMORY_LIMIT=-1
ENV COMPOSER_PROCESS_TIMEOUT=3600
ENV COMPOSER_AUTH="{\"http-basic\":{ \"gitlab.com\": { \"username\":  \"___token___\", \"password\":  \"$GITLAB_ACCESS_TOKEN\" }, \"updates.ibexa.co\":{\"username\":\"$COMPOSER_AUTH_EZ_USERNAME\",\"password\":\"$COMPOSER_AUTH_EZ_PASSWORD\"}}}"

COPY /etc/docker/app/production.ini /usr/local/etc/php/conf.d/zz-production.ini

COPY composer.json composer.loc[k] /app/

RUN composer install --prefer-dist --no-autoloader --no-suggest --no-interaction --no-scripts --no-dev


COPY assets /app/assets
COPY bin /app/bin
COPY config /app/config
COPY migrations /app/migrations
COPY public/index.php /app/public/index.php
COPY templates /app/templates
COPY translations /app/translations
COPY src /app/src
COPY package.json yarn.loc[k] ez.webpack.* webpack.config.js ./
COPY .env.default ./.env

RUN composer dump-autoload --optimize --no-interaction --classmap-authoritative && \
    composer run auto-scripts

RUN mkdir -m 0777 -p var && \
    chown -Rf www-data:www-data /app/var && chmod -R 777 /app/var

COPY /etc/docker/app/start.sh /app/start.sh
RUN chmod +x /app/start.sh

CMD [ "sh", "-c", "sh /app/start.sh && php-fpm" ]

FROM base as prod

LABEL maintainer="AXP" description="Aplyca AXP CMS" environment="Production"

##### Building Development image #####
FROM prod as dev

ENV APP_ENV=dev

RUN mv /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
COPY /etc/docker/app/development.ini /usr/local/etc/php/conf.d/zzz-development.ini

RUN composer install --prefer-source --no-autoloader --no-interaction --no-scripts && \
    composer dump-autoload --optimize --no-interaction

LABEL environment="Development"
