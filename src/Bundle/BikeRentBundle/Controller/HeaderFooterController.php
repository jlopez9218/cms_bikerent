<?php

namespace App\Bundle\BikeRentBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use Psr\Log\LoggerInterface;

use App\Bundle\BikeRentBundle\Services\ContentService;

class HeaderFooterController extends Controller
{
    private $configResolver;
    private ContentService $contentService;

    public function __construct(
        ContentService $contentService,
        ConfigResolverInterface $configResolver
    ) {
        $this->contentService = $contentService;
        $this->configResolver = $configResolver;
    }

    public function headerAction() 
    {
        try {
            $parameters = $this->configResolver->getParameter('header', 'bike');
            $content = $this->contentService->getContentByLocationId($parameters['locationId']);
            $menu = $this->contentService->getRelationsContent($content, 'menu');

            return $this->render(
                '@BikeRent/header.html.twig',
                array(
                    'content' => $content,
                    'menu' => $menu
                )
            );
        } catch (\Exception $e) {
            return new Response();
        }
    }

    public function footerAction() 
    {
        try {
            $parameters = $this->configResolver->getParameter('footer', 'bike');
            $content = $this->contentService->getContentByLocationId($parameters['locationId']);

            return $this->render(
                '@BikeRent/footer.html.twig',
                array(
                    'content' => $content
                )
            );
        } catch (\Exception $e) {
            return new Response();
        }
    }
}