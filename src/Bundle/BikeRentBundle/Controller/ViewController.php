<?php

namespace App\Bundle\BikeRentBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use eZ\Bundle\EzPublishCoreBundle\Controller;

use Psr\Log\LoggerInterface;
use App\Bundle\BikeRentBundle\Services\ContentService;
use eZ\Publish\Core\MVC\Symfony\View\View;


class ViewController extends Controller
{
    private LoggerInterface $logger;
    private ContentService $contentService;
    public function __construct(
        ContentService $contentService,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->contentService = $contentService;
    }


    public function folderAction(View $view, $locationId)
    {
        try {
            $bikes = $this->contentService->getChildrens($locationId, ['bike']);
            $view->addParameters([
                'bikes' => $bikes,
            ]);
            return $view;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }
}
