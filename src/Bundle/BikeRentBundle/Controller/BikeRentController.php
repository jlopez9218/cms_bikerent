<?php

namespace App\Bundle\BikeRentBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

class BikeRentController
{
    public function index(): Response
    {
        return new Response(
            '<html>
                <body>BikeRentBundleController</body>
            </html>'
        );
    }
}