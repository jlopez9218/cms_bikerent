<?php
/**
 * (c) www.aplyca.com
 */

namespace App\Bundle\BikeRentBundle\Services;

use \eZ\Publish\API\Repository\ContentService as eZContentService;
use \eZ\Publish\API\Repository\LocationService as eZLocationService;
use eZ\Publish\Core\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\Content;

use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\SearchService;

/**
 * It use the eZ contentService and own utils funtions.
 */
class ContentService
{

    private eZLocationService $eZlocationService;
    private eZContentService $eZContentService;

    public function __construct(
        eZContentService $eZContentService,
        eZLocationService $locationService,
        SearchService $searchService
    ) {
        $this->eZlocationService = $locationService;
        $this->eZContentService = $eZContentService;
        $this->searchService = $searchService;
    }

    public function getLocationById(int $locationId): Location
    {
        return $this->eZlocationService->loadLocation((int) $locationId);
    }
    
    /**
     * Return a content by a given location id
     */
    public function getContentByLocationId(int $locationId): Content
    {
        $location = $this->getLocationById((int) $locationId);
        return $this->eZContentService->loadContent($location->contentInfo->id);
    }
    
    public function getContentByContentInfo($contentInfo): Content
    {
        return $this->eZContentService->loadContentByContentInfo($contentInfo);
    }
    
    public function getChildrens($parentLocationId, $contentTypes, $limit = 100)
    {
        $query = new LocationQuery();
        $query->query = new Criterion\LogicalAnd(
            array(
                new Criterion\ParentLocationId($parentLocationId),
                new Criterion\ContentTypeIdentifier($contentTypes),
                new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            )
        );

        $query->sortClauses = $this->eZlocationService->loadLocation($parentLocationId)->getSortClauses();
        $query->limit = $limit;
        $results = $this->searchService->findLocations($query)->searchHits;
        
        return array_map(fn($hit) => [
            'location' => $hit->valueObject,
            'content' => $this->getContentByContentInfo($hit->valueObject->contentInfo)
        ], $results);
    }

    public function getRelationsContent(Content $content, $fieldIdentifier)
    {
        $relationContent =[];
        if (is_object($content->getFieldValue($fieldIdentifier))) {
            $relationContentIds = $content->getFieldValue($fieldIdentifier);
            foreach($relationContentIds->destinationContentIds as $relationContentId)
            {
                try{
                    $contentItem = $this->eZContentService->loadContent($relationContentId);
                    if (!$contentItem->contentInfo->isHidden) {
                        $relationContent[] = $contentItem;
                    } 
                } catch(\Exception $e) {
                    continue;
                }
            } 
        }  
        return $relationContent;
    }
}
